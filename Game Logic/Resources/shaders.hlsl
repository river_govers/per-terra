cbuffer ConstantBuffer : register( b0 )
{
	matrix World;
	matrix View;
	matrix Projection;
	float4 lightDir[2];
	float4 lightColor[2];
	float4 outputColor;
}

struct VOut
{
	float4 position : SV_POSITION;
	float4 lightColor : LIGHTCOLOR;
};

VOut VShader(float4 position : POSITION, float3 normal : NORMAL)
{
	VOut output = (VOut)0;

	output.position = mul(mul(mul(position, World), View), Projection);

	normal = mul(float4(normal, 0.0f), World).xyz;

	output.lightColor = 0;

	for (int i = 0; i < 2; i++)
	{
		output.lightColor += saturate(dot((float3)lightDir[i], normal) * lightColor[i]);
	}

	output.lightColor.a = 1.0f;

	return output;
}

// Pass in the struct it complains when you use singular variables
float4 PShader(VOut input) : SV_TARGET
{
	return input.lightColor;
}

float4 PShaderSolid() : SV_TARGET
{
	float4 finalColor = outputColor;

	return finalColor;
}