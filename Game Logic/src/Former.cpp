#include "../include/Former.h"

#include "Chunk Manager.h"
#include "Terrain.h"
#include "Maths/Maths.h"
#include "Delta Time.h"

bool Generator(int a_x, int a_y, float a_innerRadius, float a_outterRadius, float a_formingSpeed, Terrain* a_terrain, const Vector2& a_pos, float a_height)
{
	bool update = false;

	Vector3 vertPosition = a_terrain->GetIndex(a_x, a_y).pos;

	int vertCount = a_terrain->GetVerticieCount().x;

	float magSqr = (Vector2(vertPosition.x, vertPosition.z) - a_pos).MagnitudeSqr();

	if (magSqr < a_innerRadius * a_innerRadius)
	{
		Vertex& vert = a_terrain->GetIndex(a_x, a_y);

		if (vert.pos.y < a_height)
		{
			update = true;

			vert.pos.y += a_formingSpeed * (a_height - vert.pos.y);
			vert.pos.y = min(vert.pos.y, a_height);

			float hL = 0.0f;
			float hR = 0.0f;
			float hU = 0.0f;
			float hD = 0.0f;

			if (a_x - 1 >= 0 && a_x + 1 < vertCount && a_y - 1 >= 0 && a_y + 1 < vertCount)
			{
				hL = a_terrain->GetIndex(a_x - 1, a_y).pos.y;
				hR = a_terrain->GetIndex(a_x + 1, a_y).pos.y;
				hD = a_terrain->GetIndex(a_x, a_y - 1).pos.y;
				hU = a_terrain->GetIndex(a_x, a_y + 1).pos.y;

				vert.normal = (DirectX::XMFLOAT3)Vector3(hL - hR, 2.0f, hD - hU).NormalizedInv();
			}
		}
	}
	else if (magSqr < a_outterRadius * a_outterRadius)
	{
		Vertex& vert = a_terrain->GetIndex(a_x, a_y);

		float height = Lerp(a_height, -125.0f, (magSqr - (a_innerRadius * a_innerRadius)) / ((a_outterRadius * a_outterRadius) - (a_innerRadius * a_innerRadius)));
		
		if (vert.pos.y < height)
		{
			update = true;

			vert.pos.y += a_formingSpeed * (height - vert.pos.y);
			vert.pos.y = min(vert.pos.y, height);

			float hL = 0.0f;
			float hR = 0.0f;
			float hU = 0.0f;
			float hD = 0.0f;

			if (a_x - 1 >= 0 && a_x + 1 < vertCount && a_y - 1 >= 0 && a_y + 1 < vertCount)
			{
				hL = a_terrain->GetIndex(a_x - 1, a_y).pos.y;
				hR = a_terrain->GetIndex(a_x + 1, a_y).pos.y;
				hD = a_terrain->GetIndex(a_x, a_y - 1).pos.y;
				hU = a_terrain->GetIndex(a_x, a_y + 1).pos.y;

				vert.normal = (DirectX::XMFLOAT3)Vector3(hL - hR, 2.0f, hD - hU).NormalizedInv();
			}
		}
	}

	return update;
}

Former::Former(ChunkManager* a_chunkManager, Model* const a_model, float a_outerRadius, float a_innerRadius, float a_formingSpeed, PerlinNoise2D* const a_noise) : Entity(a_chunkManager, false)
{
	m_noise = a_noise;

	m_formingSpeed = a_formingSpeed;
	m_innerRadius = a_innerRadius;
	m_outerRadius = a_outerRadius;
	m_chunkManager = a_chunkManager;
	m_model = a_model;
}

void Former::Update()
{	
	if (m_updateTime >= 0.075f)
	{
		m_updateTime = 0.0f;

		Vector3 entityPos = m_pTransform->GetPosition();

		Chunk* chunk = &m_chunkManager->GetChunk({ entityPos.x, entityPos.z });

		Vector3 relativePos = entityPos - Vector3(chunk->m_pos.x, 0.0f, chunk->m_pos.y);

		float spacing = chunk->m_pTerrain->GetSpacing().x;

		int indicies = m_outerRadius / spacing;

		int vertCount = chunk->m_pTerrain->GetVerticieCount().x;

		int xMin = (int)((relativePos.x / spacing) - indicies);
		int xMax = (int)((relativePos.x / spacing) + indicies);
		int yMin = (int)((relativePos.y / spacing) - indicies);
		int yMax = (int)((relativePos.z / spacing) + indicies);

		int xStart = max(0, xMin);
		int xEnd = min(vertCount, xMax);
		int yStart = max(0, yMin);
		int yEnd = min(vertCount, yMax);

		bool update = false;

		for (int x = xStart; x < xEnd; ++x)
		{
			for (int y = yStart; y < yEnd; ++y)
			{
				if (Generator(x, y, m_innerRadius, m_outerRadius, m_formingSpeed, chunk->m_pTerrain, { relativePos.x, relativePos.z }, (float)m_noise->Noise(chunk->m_pos.x + x * spacing, chunk->m_pos.y + y * spacing)))
				{
					update = true;
				}
			}
		}

		if (update)
		{
			chunk->m_pTerrain->UpdateBuffer();
		}

		if (xMin < 0)
		{
			float size = (m_chunkManager->GetChunkSize().x - spacing);

			chunk = &m_chunkManager->GetChunk({ entityPos.x - size, entityPos.z });

			relativePos = entityPos - Vector3(chunk->m_pos.x, 0.0f, chunk->m_pos.y);

			bool update = false;

			for (int x = vertCount + xMin; x < vertCount; ++x)
			{
				for (int y = yStart; y < yEnd; ++y)
				{
					if (Generator(x, y, m_innerRadius, m_outerRadius, m_formingSpeed, chunk->m_pTerrain, { relativePos.x, relativePos.z }, (float)m_noise->Noise(chunk->m_pos.x + x * spacing, chunk->m_pos.y + y * spacing)))
					{
						update = true;
					}
				}
			}

			if (update)
			{
				chunk->m_pTerrain->UpdateBuffer();
			}
		}
		if (xMax > vertCount)
		{
			float size = (m_chunkManager->GetChunkSize().x - spacing);

			chunk = &m_chunkManager->GetChunk({ entityPos.x + size, entityPos.z });

			relativePos = entityPos - Vector3(chunk->m_pos.x, 0.0f, chunk->m_pos.y);

			bool update = false;

			for (int x = 0; x < xMax - vertCount; ++x)
			{
				for (int y = yStart; y < yEnd; ++y)
				{
					if (Generator(x, y, m_innerRadius, m_outerRadius, m_formingSpeed, chunk->m_pTerrain, { relativePos.x, relativePos.z }, (float)m_noise->Noise(chunk->m_pos.x + x * spacing, chunk->m_pos.y + y * spacing)))
					{
						update = true;
					}
				}
			}

			if (update)
			{
				chunk->m_pTerrain->UpdateBuffer();
			}
		}
		if (yMin < 0)
		{
			float size = (m_chunkManager->GetChunkSize().x - spacing);

			chunk = &m_chunkManager->GetChunk({ entityPos.x, entityPos.z - size });

			relativePos = entityPos - Vector3(chunk->m_pos.x, 0.0f, chunk->m_pos.y);

			bool update = false;

			for (int x = xStart; x < xEnd; ++x)
			{
				for (int y = vertCount + yMin; y < vertCount; ++y)
				{
					if (Generator(x, y, m_innerRadius, m_outerRadius, m_formingSpeed, chunk->m_pTerrain, { relativePos.x, relativePos.z }, (float)m_noise->Noise(chunk->m_pos.x + x * spacing, chunk->m_pos.y + y * spacing)))
					{
						update = true;
					}
				}
			}

			if (update)
			{
				chunk->m_pTerrain->UpdateBuffer();
			}
		}
		if (yMax > vertCount)
		{
			float size = (m_chunkManager->GetChunkSize().x - spacing);

			chunk = &m_chunkManager->GetChunk({ entityPos.x, entityPos.z + size });

			relativePos = entityPos - Vector3(chunk->m_pos.x, 0.0f, chunk->m_pos.y);

			bool update = false;

			for (int x = xStart; x < xEnd; ++x)
			{
				for (int y = 0; y < yMax - vertCount; ++y)
				{
					if (Generator(x, y, m_innerRadius, m_outerRadius, m_formingSpeed, chunk->m_pTerrain, { relativePos.x, relativePos.z }, (float)m_noise->Noise(chunk->m_pos.x + x * spacing, chunk->m_pos.y + y * spacing)))
					{
						update = true;
					}
				}
			}

			if (update)
			{
				chunk->m_pTerrain->UpdateBuffer();
			}
		}
		if (xMin < 0 && yMin < 0)
		{
			float size = (m_chunkManager->GetChunkSize().x - spacing);

			chunk = &m_chunkManager->GetChunk({ entityPos.x - size, entityPos.z - size });

			relativePos = entityPos - Vector3(chunk->m_pos.x, 0.0f, chunk->m_pos.y);

			bool update = false;

			for (int x = vertCount + xMin; x < vertCount; ++x)
			{
				for (int y = vertCount + yMin; y < vertCount; ++y)
				{
					if (Generator(x, y, m_innerRadius, m_outerRadius, m_formingSpeed, chunk->m_pTerrain, { relativePos.x, relativePos.z }, (float)m_noise->Noise(chunk->m_pos.x + x * spacing, chunk->m_pos.y + y * spacing)))
					{
						update = true;
					}
				}
			}

			if (update)
			{
				chunk->m_pTerrain->UpdateBuffer();
			}
		}
		if (xMax > vertCount && yMin < 0)
		{
			float size = (m_chunkManager->GetChunkSize().x - spacing);

			chunk = &m_chunkManager->GetChunk({ entityPos.x + size, entityPos.z - size });

			relativePos = entityPos - Vector3(chunk->m_pos.x, 0.0f, chunk->m_pos.y);

			bool update = false;

			for (int x = 0; x < xMax - vertCount; ++x)
			{
				for (int y = vertCount + yMin; y < vertCount; ++y)
				{
					if (Generator(x, y, m_innerRadius, m_outerRadius, m_formingSpeed, chunk->m_pTerrain, { relativePos.x, relativePos.z }, (float)m_noise->Noise(chunk->m_pos.x + x * spacing, chunk->m_pos.y + y * spacing)))
					{
						update = true;
					}
				}
			}

			if (update)
			{
				chunk->m_pTerrain->UpdateBuffer();
			}
		}
		if (xMax > vertCount && yMax > vertCount)
		{
			float size = (m_chunkManager->GetChunkSize().x - spacing);

			chunk = &m_chunkManager->GetChunk({ entityPos.x + size, entityPos.z + size });

			relativePos = entityPos - Vector3(chunk->m_pos.x, 0.0f, chunk->m_pos.y);

			bool update = false;

			for (int x = 0; x < xMax - vertCount; ++x)
			{
				for (int y = 0; y < yMax - vertCount; ++y)
				{
					if (Generator(x, y, m_innerRadius, m_outerRadius, m_formingSpeed, chunk->m_pTerrain, { relativePos.x, relativePos.z }, (float)m_noise->Noise(chunk->m_pos.x + x * spacing, chunk->m_pos.y + y * spacing)))
					{
						update = true;
					}
				}
			}
			
			if (update)
			{
				chunk->m_pTerrain->UpdateBuffer();
			}
		}
		if (xMin < 0 && yMax > vertCount)
		{
			float size = (m_chunkManager->GetChunkSize().x - spacing);

			chunk = &m_chunkManager->GetChunk({ entityPos.x - size, entityPos.z + size });

			relativePos = entityPos - Vector3(chunk->m_pos.x, 0.0f, chunk->m_pos.y);

			bool update = false;

			for (int x = vertCount + xMin; x < vertCount; ++x)
			{
				for (int y = 0; y < yMax - vertCount; ++y)
				{
					if (Generator(x, y, m_innerRadius, m_outerRadius, m_formingSpeed, chunk->m_pTerrain, { relativePos.x, relativePos.z }, (float)m_noise->Noise(chunk->m_pos.x + x * spacing, chunk->m_pos.y + y * spacing)))
					{
						update = true;
					}
				}
			}

			if (update)
			{
				chunk->m_pTerrain->UpdateBuffer();
			}
		}
	}

	m_updateTime += Time::GetDeltaTime();
}
