#include "../include/World.h"

#include "Application.h"
#include "Pipeline.h"

using namespace DirectX;

#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBGUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBGUG_NEW
#endif 

GraphNode<World::NodeData*>* World::GetNearestNode(const XMFLOAT2& a_pos)
{
	GraphNode<NodeData*>* node = nullptr;
	float distSqr = std::numeric_limits<float>().infinity();

	for (auto iter = m_terrain.m_nodes.begin(); iter != m_terrain.m_nodes.end(); iter++)
	{
		XMFLOAT2 vecBet = { iter->data->pos.x - a_pos.x, iter->data->pos.y - a_pos.y };

		float distTemp = vecBet.x * vecBet.x + vecBet.y * vecBet.y;

		if (distTemp < distSqr)
		{
			distSqr = distTemp;
			node = &(*iter);
		}
	}

	return node;
}

World::World(const XMINT2& a_indexes, const XMINT2& a_resolution, const XMFLOAT2& a_size)
{
	m_size = a_indexes;

	std::vector<std::vector<GraphNode<NodeData*>*>> terrainStorage;

	terrainStorage.reserve(a_indexes.x);

	XMFLOAT2 scale = { a_size.x / a_resolution.x, a_size.y / a_resolution.y };

	for (int x = 0; x < a_indexes.x; x++)
	{
		terrainStorage.push_back(std::vector<GraphNode<NodeData*>*>());
		terrainStorage[x].reserve(a_indexes.y);

		for (int y = 0; y < a_indexes.y; y++)
		{
			terrainStorage[x].push_back(m_terrain.AddNode(new NodeData(new Terrain(a_resolution, a_size), XMFLOAT2(x * (a_size.x - scale.x), y * (a_size.y - scale.y)))));
		}
	}

	for (int x = 0; x < a_indexes.x; x++)
	{
		for (int y = 0; y < a_indexes.y; y++)
		{
			if (x - 1 >= 0)
			{
				m_terrain.AddEdge(*terrainStorage[x][y], *terrainStorage[x - 1][y]);
			}
			if (x + 1 < a_indexes.x)
			{
				m_terrain.AddEdge(*terrainStorage[x][y], *terrainStorage[x + 1][y]);
			}

			if (y - 1 >= 0)
			{
				m_terrain.AddEdge(*terrainStorage[x][y], *terrainStorage[x][y - 1]);
			}
			if (y + 1 < a_indexes.y)
			{
				m_terrain.AddEdge(*terrainStorage[x][y], *terrainStorage[x][y + 1]);
			}
		}
	}
}

World::~World()
{
	for (auto iter = m_terrain.m_nodes.begin(); iter != m_terrain.m_nodes.end(); iter++)
	{
		if (iter->data)
		{
			if (iter->data->terrain)
			{
				delete iter->data->terrain;
			}
			delete iter->data;
		}
	}
}

Terrain& World::GetTerrain(const XMFLOAT2& a_pos)
{
	return *GetNearestNode(a_pos)->data->terrain;
}

XMINT2 World::GetSize() const
{
	return m_size;
}

void World::Draw(const DirectX::XMFLOAT2& a_position, int a_gCap)
{
	GraphNode<World::NodeData*>* node = GetNearestNode(a_position);

	std::stack<GraphNode<World::NodeData*>*> stack;
	stack.push(node);
	node->onStack = true;
	node->data->gScore = 0;

	while (!stack.empty())
	{
		node = stack.top();
		stack.pop();
		node->onStack = false;

		if (node->data->gScore < a_gCap)
		{
			for (auto iter = node->edges.begin(); iter != node->edges.end(); iter++)
			{
				if (!iter->end->traversed && !iter->end->onStack)
				{
					stack.push(iter->end);
					iter->end->onStack = true;
					iter->end->data->gScore = node->data->gScore + 1; 
				}
			}
		}

		node->traversed = true;
		
		//Application::GetActivePipeline()->DrawTerrain(XMMatrixTranslation(node->data->pos.x, 0.0f, node->data->pos.y), *node->data->terrain);
		Application::GetActivePipeline()->DrawTerrainWireFrame(XMMatrixTranslation(node->data->pos.x, 0.001f, node->data->pos.y), *node->data->terrain, { 0.0f, 1.0f, 0.0f, 1.0f });
	}

	for (auto iter = m_terrain.m_nodes.begin(); iter != m_terrain.m_nodes.end(); iter++)
	{
		iter->onStack = false;
		iter->traversed = false;
		iter->data->gScore = 0;
	}
}
