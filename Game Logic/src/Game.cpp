#include "../include/Game.h"

#include "Pipeline.h"

#include "Input.h"

#include "Delta Time.h"
//#include <time.h>
#include <thread>

#include "Perlin2D.h"

#include "Camera.h"

#include "Maths/Vector2.h"

#define WORLD_SIZE 10
//#define WORLD_SIZE 250

#include "Terrain.h"

//void Generate(World* world, NavMesh*& navMesh, std::list<Entity*>& entities, Model* tree)
//{
//	using namespace DirectX;
//
//	for (auto iter = entities.begin(); iter != entities.end(); iter++)
//	{
//		delete *iter;
//	}
//
//	entities.clear();
//
//	Terrain* terrain = &world->GetTerrain({ 0.0f, 0.0f });
//
//	XMFLOAT2 scale = terrain->GetSpacing();
//	XMFLOAT2 size = { terrain->GetSize().x - scale.x, terrain->GetSize().y - scale.y };
//	XMINT2 res = terrain->GetVerticeCount();
//
//	int chance = 10000;	
//
//	float randomNoise = 1.0f;
//
//	for (int cX = 0; cX < WORLD_SIZE; cX++)
//	{
//		for (int cY = 0; cY < WORLD_SIZE; cY++)
//		{
//			XMFLOAT2 pos = { size.x * cX, size.y * cY };
//
//			terrain = &world->GetTerrain(pos);
//			for (int x = 0; x < res.x; x++)
//			{
//				for (int y = 0; y < res.y; y++)
//				{
//					terrain->GetIndex(x, y).pos.y = ((rand() / (RAND_MAX / randomNoise)) - 1) * ((rand() / (RAND_MAX / randomNoise)) - 1);
//				}
//			}
//		}
//	}
//
//	double amplitude = 150.0;
//	float octaveMulti = 0.5f;
//
//	for (int passes = 0; passes < 8; passes++)
//	{
//		PerlinNoise2D noise = PerlinNoise2D((int)time(NULL) * time(NULL), (float)amplitude, 0.00015f); 
//		PerlinNoise2D additionNoise = PerlinNoise2D((int)time(NULL) >> 2, 2.5f, 0.0015f);
//
//		for (int cX = 0; cX < WORLD_SIZE; cX++)
//		{
//			for (int cY = 0; cY < WORLD_SIZE; cY++)
//			{
//				XMFLOAT2 pos = { size.x * cX, size.y * cY };
//
//				terrain = &world->GetTerrain(pos);
//				for (int x = 0; x < res.x; x++)
//				{
//					for (int y = 0; y < res.y; y++)
//					{
//						/*float height = (float)((noise.Noise(pos.x + (x * scale.x), pos.y + (y * scale.y)) - amplitude / 2) + terrain->GetIndex(x, y).pos.y) / 2.0f;*/
//
//						float height = (float)(((noise.OctaveNoise(pos.x + (x * scale.x), pos.y + (y * scale.y), (int)ceil((passes + 1) * octaveMulti), 5) - amplitude / 2.0f) + terrain->GetIndex(x, y).pos.y) / additionNoise.Noise(pos.x + (x * scale.x), pos.y + (y * scale.y)));
//
//						terrain->GetIndex(x, y).pos.y = height;
//					}
//				}
//			}
//		}	
//	}
//
//	Terrain* leftTerrain = nullptr;
//	Terrain* rightTerrain = nullptr;
//	Terrain* topTerrain = nullptr;
//	Terrain* bottomTerrain = nullptr;
//
//	for (int cX = 0; cX < WORLD_SIZE; cX++)
//	{
//		for (int cY = 0; cY < WORLD_SIZE; cY++)
//		{
//			XMFLOAT2 pos = { size.x * cX, size.y * cY };
//			
//			if (cX + 1 < WORLD_SIZE)
//			{
//				rightTerrain = &world->GetTerrain({ size.x * (cX + 1), size.y * cY });
//			}
//			else
//			{
//				rightTerrain = nullptr;
//			}
//
//			if (cX - 1 >= 0)
//			{
//				leftTerrain = &world->GetTerrain({ size.x * (cX - 1), size.y * cY });
//			}
//			else
//			{
//				leftTerrain = nullptr;
//			}
//
//			if (cY + 1 < WORLD_SIZE)
//			{
//				topTerrain = &world->GetTerrain({ size.x, size.y * (cY + 1) });
//			}
//			else
//			{
//				topTerrain = nullptr;
//			}
//
//			if (cY - 1 >= 0)
//			{
//				bottomTerrain = terrain;
//			}
//			else
//			{
//				bottomTerrain = nullptr;
//			}
//
//			terrain = &world->GetTerrain(pos);
//
//			for (int x = 0; x < res.x; x++)
//			{
//				for (int y = 0; y < res.y; y++)
//				{
//					bool left = false;
//					Vector3 leftPos;
//
//					if (x - 1 >= 0)
//					{
//						left = true;
//						leftPos = terrain->GetIndex(x, y).pos;
//					}
//					else if (leftTerrain)
//					{
//						left = true;
//						leftPos = leftTerrain->GetIndex(res.x - 2, y).pos;
//
//						float height = (terrain->GetIndex(x, y).pos.y + leftTerrain->GetIndex(res.x - 1, y).pos.y) / 2.0f;
//
//						leftTerrain->GetIndex(res.x - 1, y).pos.y = height;
//						terrain->GetIndex(x, y).pos.y = height;
//					}
//
//					bool right = false;
//					Vector3 rightPos;
//
//					if (x + 1 < res.x)
//					{
//						right = true;
//						rightPos = terrain->GetIndex(x, y).pos;
//					}
//					else if (rightTerrain)
//					{
//						right = true;
//						rightPos = rightTerrain->GetIndex(1, y).pos;
//					}
//
//					bool bottom = false;
//					Vector3 bottomPos;
//
//					if (y - 1 >= 0)
//					{
//						bottom = true;
//						bottomPos = terrain->GetIndex(x, y - 1).pos;
//					}
//					else if (bottomTerrain)
//					{
//						bottom = true;
//						bottomPos = bottomTerrain->GetIndex(x, res.y - 2).pos;
//
//						float height = (terrain->GetIndex(x, y).pos.y + bottomTerrain->GetIndex(x, res.y - 1).pos.y) / 2.0f;
//
//						bottomTerrain->GetIndex(x, res.y - 1).pos.y = height;
//						terrain->GetIndex(x, y).pos.y = height;
//					}
//
//					bool top = false;
//					Vector3 topPos;
//
//					if (y + 1 < WORLD_SIZE)
//					{
//						top = true;
//						topPos = terrain->GetIndex(x, y + 1).pos;
//					}
//					else if (topTerrain)
//					{
//						top = true;
//						topPos = terrain->GetIndex(x, 2).pos;
//					}
//
//					//Vector3 verticalCross = Vector3::Up();
//
//					//if (top && bottom)
//					//{
//					//	/*verticalCross = topPos.Cross(bottomPos);*/
//					//	verticalCross = topPos + bottomPos;
//					//}
//					//
//					//Vector3 horizontalCross = Vector3::Up();
//
//					//if (left && right)
//					//{
//					//	/*horizontalCross = leftPos.Cross(rightPos);*/
//					//	horizontalCross = leftPos = rightPos;
//					//}
//
//					///*terrain->GetIndex(x, y).normal = (XMFLOAT3)(verticalCross.Cross(horizontalCross).Normalized());*/
//					///*terrain->GetIndex(x, y).normal = (XMFLOAT3)((verticalCross + horizontalCross).NormalizedInv());*/
//					terrain->GetIndex(x, y).normal = (XMFLOAT3)Vector3::Down();
//				}
//			}
//		}
//	}
//
//	for (int cX = 0; cX < WORLD_SIZE; cX++)
//	{
//		for (int cY = 0; cY < WORLD_SIZE; cY++)
//		{
//			XMFLOAT2 pos = { size.x * cX, size.y * cY };
//			terrain = &world->GetTerrain(pos);
//
//			terrain->UpdateBuffer();
//		}
//	}
//
//	/*if (navMesh)
//	{
//		delete navMesh;
//	}*/
//
//	/*std::list<Obstacle> obstacles;
//
//	for (auto iter = entities.begin(); iter != entities.end(); iter++)
//	{
//		obstacles.push_back({ (*iter)->GetPosition(), 3.5f });
//	}
//
//	navMesh = new NavMesh(world->GetTerrain({ 0.0f, 0.0f }), 2.5f, obstacles);*/
//}

Game::Game(int a_screenWidth, int a_screenHeight, const HINSTANCE& a_instance, int a_nCmdShow, const char* a_pApplicationName) : Application(a_screenWidth, a_screenHeight, a_instance, a_nCmdShow, a_pApplicationName)
{
	new Camera(true, DirectX::XM_PIDIV2);
	Vector2 camSize = Vector2(320.0f, 180.0f);
	m_secondaryCamera = new Camera(false, GetActivePipeline()->GetViewDimensions() - camSize, camSize, DirectX::XM_PIDIV2);

	m_secondaryCamera->Transform().SetLocalRotation(Vector3(DirectX::XM_PIDIV2 - DirectX::XM_PIDIV4 / 4.0f, DirectX::XM_PIDIV4, 0.0f));

	speed = 10.0f;

	Model::CreateIcoSphere(m_node);

	//m_pChunkManager = new ChunkManager(WORLD_SIZE, 32.0f, 16);
	m_pChunkManager = new ChunkManager(WORLD_SIZE, 48.0f, 64);

	m_pNoise = new PerlinNoise2D((int)time(NULL), 7.5f, 0.025f);

	//m_pEntity = new Former(m_pChunkManager, m_node, 15.0f, 9.0f, 0.25f, m_pNoise);
	//m_pEntity = new Former(m_pChunkManager, m_node, 25.0f, 17.5f, 0.2f, m_pNoise);
	//m_pEntity->m_pTransform->SetLocalPosition(Vector3(15.0f, 0.0f, 15.0f));

#pragma region Generate
	for (int cX = 0; cX < WORLD_SIZE; ++cX)
	{
		for (int cY = 0; cY < WORLD_SIZE; ++cY)
		{
			Terrain* terrain = m_pChunkManager->GetChunk(cX, cY).m_pTerrain;

			for (int x = 0; x < terrain->GetVerticieCount().x; ++x)
			{
				for (int y = 0; y < terrain->GetVerticieCount().y; ++y)
				{
					terrain->GetIndex(x, y).pos.y = -125.0f;
					//terrain->GetIndex(x, y).pos.y = 0.0f;
					terrain->GetIndex(x, y).normal = { 0.0f, 1.0f, 0.0f };
				}
			}

			terrain->UpdateBuffer();
		}
	}
#pragma endregion
}
Game::~Game()
{
	//delete m_world;

	//delete m_pEntity;

	for (auto iter = m_formers.begin(); iter != m_formers.end(); ++iter)
	{
		delete iter->former;
	}

	delete m_pNoise;

	delete m_node;

	delete m_pChunkManager;
}

void Game::Update()
{
	Vector3 mov = { 0.0f, 0.0f, 0.0f };
	Vector3 rot = { 0.0f, 0.0f, 0.0f };

	if (Input::GetSingleton()->IsKeyDown(e_key::S))
	{
		mov.z -= 1.0f;
	}
	if (Input::GetSingleton()->IsKeyDown(e_key::W))
	{
		mov.z += 1.0f;
	}
	if (Input::GetSingleton()->IsKeyDown(e_key::A))
	{
		mov.x -= 1.0f;
	}
	if (Input::GetSingleton()->IsKeyDown(e_key::D))
	{
		mov.x += 1.0f;
	}

	if (Input::GetSingleton()->IsKeyDown(e_key::Space))
	{
		mov.y += 1.0f;
	}
	if (Input::GetSingleton()->IsKeyDown(e_key::C))
	{
		mov.y -= 1.0f;
	}

	if (Input::GetSingleton()->IsKeyDown(e_key::Q))
	{
		rot.z -= 1.0f;
	}
	if (Input::GetSingleton()->IsKeyDown(e_key::E))
	{
		rot.z += 1.0f;
	}

	if (Input::GetSingleton()->IsMousePressed(e_mouseButton::Left))
	{
		Input::GetSingleton()->SetCursorLock(true);
		Input::GetSingleton()->SetCursorVisiblity(false);
	}
	if (Input::GetSingleton()->IsKeyPressed(e_key::Escape))
	{
		Input::GetSingleton()->SetCursorLock(false);
		Input::GetSingleton()->SetCursorVisiblity(true);
	}

	if (Input::GetSingleton()->IsKeyDown(e_key::Shift))
	{
		speed = 100.0f;
	}
	else
	{
		speed = 10.0f;
	}

	Vector2 mouseShift;

	if (Input::GetSingleton()->GetCursorLockState())
	{
		mouseShift = Input::GetSingleton()->GetMouseMovement();

		rot.x = -mouseShift.y;
		rot.y = -mouseShift.x;
	}

	Transform& transform = Camera::GetActiveCamera().Transform();

	if (Input::GetSingleton()->IsMousePressed(e_mouseButton::Right))
	{
		Vector3 camPos = transform.GetLocalPosition();

		Former* former = new Former(m_pChunkManager, m_node, 25.0f, 12.5f, 0.00125f, m_pNoise);
		former->m_pTransform->SetLocalPosition(Vector3(camPos.x, 0.0f, camPos.z));

		m_formers.push_back(FormerCon(former));

		//m_pEntity->m_pTransform->SetLocalPosition(Vector3(camPos.x, 0.0f, camPos.z));
	}

	Vector3 forward = transform.Forward();
	Vector3 up = transform.Up();
	Vector3 right = transform.Right();

	transform.Translate(forward * mov.z * speed * Time::GetDeltaTime());
	transform.Translate(up * mov.y * speed * Time::GetDeltaTime());
	transform.Translate(right * mov.x * speed * Time::GetDeltaTime());

	transform.Rotate({ rot.x * 0.008f, rot.y * 0.008f, rot.z * Time::GetDeltaTime() });

	Vector3 pos = transform.GetLocalPosition();

	transform.SetLocalPosition({ min(WORLD_SIZE * 47.25f - 1.0f, max(1.0f, pos.x)), pos.y, min(WORLD_SIZE * 47.25f - 1.0f, max(1.0f, pos.z)) });

	m_secondaryCamera->Transform().SetLocalPosition(Vector3(transform.GetLocalPosition().x, 10.0f, transform.GetLocalPosition().z));

	for (auto iter = m_formers.begin(); iter != m_formers.end(); ++iter)
	{
		if (iter->timePassed > 60.0f)
		{
			delete iter->former;
			m_formers.erase(iter);

			break;
		}

		iter->timePassed += Time::GetDeltaTime();
		iter->former->Update();
	}

	//m_pChunkManager->Update();
}

void Game::Draw(Pipeline* a_pPipeline)
{
	Vector3 camPos = Camera::GetActiveCamera().Transform().GetLocalPosition();
	
	if (Input::GetSingleton()->IsKeyUp(e_key::F))
	{
		m_pChunkManager->Draw(5, { camPos.x, camPos.z });
	}
	else
	{
		m_pChunkManager->DrawWireFrame(5, { camPos.x, camPos.z });
	}
}
void Game::LineDraw(Pipeline* a_pPipeline)
{
	Vector3 camPos = Camera::GetActiveCamera().Transform().GetLocalPosition();

	if (Input::GetSingleton()->IsKeyDown(e_key::F))
	{
		m_pChunkManager->DrawDebug(5, { camPos.x, camPos.z });

		Chunk& chunk = m_pChunkManager->GetChunk({ camPos.x, camPos.z });

		for (int i = 0; i < chunk.m_pTerrain->GetVerticieCount().x; ++i)
		{
			for (int j = 0; j < chunk.m_pTerrain->GetVerticieCount().y; ++j)
			{
				Vertex vert = chunk.m_pTerrain->GetIndex(i, j);

				a_pPipeline->DrawLine(Vector3(chunk.m_pos.x, 0.0f, chunk.m_pos.y) + vert.pos, Vector3(chunk.m_pos.x, 0.0f, chunk.m_pos.y) + Vector3(vert.pos) + Vector3(vert.normal), { 0.0f, 0.0f, 1.0f, 1.0f });
			}
		}
	}

	Vector3 point = Camera::GetActiveCamera().ScreenToWorld(Vector3(a_pPipeline->GetViewDimensions()) - Vector3(250.0f, 200.0f, -0.5f));

	a_pPipeline->DrawLine(point, point + Vector3::Forward() * 0.0025f, D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
	a_pPipeline->DrawLine(point, point + Vector3::Up()		* 0.0025f, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
	a_pPipeline->DrawLine(point, point + Vector3::Right()	* 0.0025f, D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f));
}
