#include <Windows.h>

#define _SCREEN_WIDTH_ 1280
#define _SCREEN_HEIGHT_ 720

//#define _SCREEN_WIDTH_ 1920
//#define _SCREEN_HEIGHT_ 1080

#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBGUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBGUG_NEW
#endif 

#include "../include/Game.h"

#pragma comment(lib, "PerTerra Framework.lib")

#include <time.h>

int WINAPI WinMain(HINSTANCE a_hinstance, HINSTANCE a_hPrevInstance, LPSTR a_lpCmdLine, int nCmdShow)
{
	srand((unsigned int)time(NULL));

#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif // _DEBUG

	Application* app = new Game(_SCREEN_WIDTH_, _SCREEN_HEIGHT_, a_hinstance, nCmdShow, "PerTerra Test");
	int exit = app->Run();
	//int exit = 0;

	delete app;

	return exit;
}