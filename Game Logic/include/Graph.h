#pragma once

#include <list>
#include <vector>
#include <stack>

#include "../include/Model.h"

template<typename T>
struct GraphEdge;
template<typename T>
struct GraphNode;

template<typename T>
struct GraphNode
{
	bool traversed;
	bool onStack;
	std::list<GraphEdge<T>> edges;
	
	T data;

	GraphNode(const T& a_data)
	{
		data = a_data;
		traversed = false;
		onStack = false;
	}
	GraphNode(const GraphNode<T>& a_node)
	{
		traversed = a_node.traversed;
		onStack = a_node.onStack;
		edges = a_node.edges;

		data = a_node.data;
	}
	
	bool operator >(const GraphNode<T>& a_graphNode) const
	{
		return data > a_graphNode.data;
	}
	bool operator <(const GraphNode<T>& a_graphNode) const
	{
		return data < a_graphNode.data;
	}
};

template<typename T>
struct GraphEdge
{
	GraphNode<T>* start;
	GraphNode<T>* end;

	float cost;

	GraphEdge(GraphNode<T>& a_start, GraphNode<T>& a_end, float a_cost = 0.0f)
	{
		start = &a_start;
		end = &a_end;

		cost = a_cost;
	}
};

template<typename T>
class Graph
{
private:
	friend class World;
	friend class NavGraph;
	friend class HNavGraph;
	friend class NavMesh;
	std::list<GraphNode<T>> m_nodes;
protected:

public:
	GraphNode<T>* AddNode(const T& a_data)
	{
		m_nodes.push_back(GraphNode<T>(a_data));

		return &(*(--m_nodes.end()));
	}
	GraphEdge<T>* AddEdge(GraphNode<T>& a_start, GraphNode<T>& a_end)
	{
		a_start.edges.push_back(GraphEdge<T>(a_start, a_end));

		return &(*(--a_start.edges.end()));
	}

	void RemoveNode(const GraphNode<T>& a_node)
	{
		std::stack<GraphNode<T>> stack;

		stack.push(m_nodes.begin());

		m_nodes.begin().onStack = true;

		GraphNode<T> node;
		while (stack.size())
		{ 
			node = stack.pop();

			for (auto iter = node->edges.begin(); iter != node->edges.end(); ++iter)
			{
				if (*iter->start == a_node)
				{
					node->edges.remove(iter);
				}
				else if(!iter->end->traversed && !iter->end->onStack)
				{
					stack.push(*iter->end);
					iter->end->onStack = true;
				}
			}

			node.onStack = false; 
			node.traversed = true;
		}

		for (int i = 0; i < m_nodes.size(); i++)
		{
			m_nodes[i].traversed = false;
		}
	}
	void RemoveEdge(const GraphEdge<T>& a_edge)
	{
		std::stack<GraphNode<T>> stack;

		stack.push(m_nodes[0]);

		m_nodes[0].onStack = true;

		GraphNode<T> node;

		bool found = false;

		while (!stack.empty() && !found)
		{
			node = stack.pop();

			for (auto iter = node->edges.begin(); iter != node->edges.end(); ++iter)
			{
				if (*iter == a_edge)
				{
					node->edges.remove(iter);

					found = true;
					break;
				}
				else if (!iter->end->traversed && !iter->end->onStack)
				{
					stack.push(*iter->end);
					iter->end->onStack = true;
				}
			}

			node.onStack = false;
			node.traversed = true;
		}

		for (int i = 0; i < m_nodes.size(); i++)
		{
			m_nodes[i].traversed = false;
			m_nodes[i].onStack = false;
		}
	}

	std::list<GraphNode<T>>& GetNodes()
	{
		return m_nodes;
	}
};

