#pragma once

#include "../include/Application.h"

#include "../include/Model.h"

#include "Chunk Manager.h"

#include "Entity.h"
#include "Former.h"

class Camera;

struct FormerCon
{
	float timePassed;
	Former* former;

	FormerCon(Former* a_former) : timePassed(0.0f), former(a_former)
	{	
	}
};

class Game : public Application
{
private:
	PerlinNoise2D* m_pNoise;

	ChunkManager* m_pChunkManager;

	Model* m_node = nullptr;

	int seed;

	float speed;

	Camera* m_secondaryCamera;

	std::list<FormerCon> m_formers;
protected:

public:
	Game(int a_screenWidth, int a_screenHeight, const HINSTANCE& a_instance, int a_nCmdShow, const char* a_pApplicationName);
	~Game();

	void Update();
	void Draw(Pipeline* a_pPipeline);
	void LineDraw(Pipeline* a_pPipeline);
};