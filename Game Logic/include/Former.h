#pragma once

#include "Entity.h"

#include "Perlin2D.h"

class Former : public Entity
{
private:
	ChunkManager* m_chunkManager;

	float m_outerRadius;
	float m_innerRadius;

	float m_formingSpeed;

	PerlinNoise2D* m_noise;

	float m_updateTime;
protected:

public:
	Former(ChunkManager* a_chunkManager, Model* const a_model, float a_outerRadius, float a_innerRadius, float a_formingSpeed, PerlinNoise2D* const a_noise);

	void Update();
};