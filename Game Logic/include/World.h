#pragma once

#include <vector>
#include <DirectXMath.h>

#include "../include/Graph.h"
#include "../include/Terrain.h"

class World
{
private:
	struct NodeData
	{
		int gScore;
	
		Terrain* terrain;
		DirectX::XMFLOAT2 pos;

		NodeData(Terrain* a_terrain, const DirectX::XMFLOAT2& a_pos)
		{
			gScore = 2147483647;

			terrain = a_terrain;
			pos = a_pos;
		}
	};

	DirectX::XMINT2 m_size;

	Graph<NodeData*> m_terrain;

	GraphNode<NodeData*>* GetNearestNode(const DirectX::XMFLOAT2& a_pos);
protected:

public:
	World(const DirectX::XMINT2& a_indexes, const DirectX::XMINT2& a_resolution, const DirectX::XMFLOAT2& a_size);
	~World();

	Terrain& GetTerrain(const DirectX::XMFLOAT2& a_pos);

	DirectX::XMINT2 GetSize() const;

	void Draw(const DirectX::XMFLOAT2& a_position, int a_gCap);
};