#include "..\include\Application.h"

#include "..\include\Pipeline.h"
#include "..\include\Input.h"

#include "..\include\Delta Time.h"

#include <thread>

#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBGUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBGUG_NEW
#endif 

Application* Application::m_pActiveApplication = nullptr;

Application::Application(int a_screenWidth, int a_screenHeight, const HINSTANCE& a_instance, int a_nCmdShow, const char* a_pApplicationName)
{
	m_pActiveApplication = this;

	WNDCLASSEX windowClass;

	ZeroMemory(&windowClass, sizeof(WNDCLASSEX));

	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc = WindowProc;
	windowClass.hInstance = a_instance;
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.lpszClassName = a_pApplicationName;

	RegisterClassEx(&windowClass);

	RECT windowRect = { 0, 0, a_screenWidth, a_screenHeight };

	AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

	m_windowPos.x = 300;
	m_windowPos.y = 300;

	m_window = CreateWindowEx(NULL,
							  a_pApplicationName,				   // Class Name
							  a_pApplicationName,				   // Window Name
							  WS_OVERLAPPEDWINDOW,				   // Window Style
							  (int)m_windowPos.x,				   // X Coordinate
							  (int)m_windowPos.y,				   // Y Coordinate
							  windowRect.right - windowRect.left,  // X Size
							  windowRect.bottom - windowRect.top,  // Y Size
							  NULL,								   // Parent Window
							  NULL,								   // Menus
							  a_instance,						   // Application Handle
							  NULL);							   // Muli Windowed

	ShowWindow(m_window, a_nCmdShow);

	m_pPipeline = new Pipeline(m_window, a_screenWidth, a_screenHeight);

	Input::CreateInputManager();
}
Application::~Application()
{
	delete m_pPipeline;

	Input::DestroyInputManager();
}

int Application::Run()
{
	if (!m_pPipeline->m_success)
	{
		return 0;
	}

	MSG msg;

	ULONGLONG timeStart = GetTickCount64();
	ULONGLONG timeCur = GetTickCount64();
	
	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			bool handled = false;

			switch (msg.message)
			{
			case WM_QUIT:
			{
				return msg.wParam;
			}
			case WM_KEYDOWN:
			{
				Input::m_pInput->OnKeyDown(msg.wParam);
				handled = true;
				break;
			}
			case WM_KEYUP:
			{
				Input::m_pInput->OnKeyUp(msg.wParam);
				handled = true;
				break;
			}
			case WM_LBUTTONDOWN:
			{
				Input::m_pInput->OnMouseDown(e_mouseButton::Left);
				handled = true;
				break;
			}
			case WM_LBUTTONUP:
			{
				Input::m_pInput->OnMouseUp(e_mouseButton::Left);
				handled = true;
				break;
			}
			case WM_MBUTTONDOWN:
			{
				Input::m_pInput->OnMouseDown(e_mouseButton::Middle);
				handled = true;
				break;
			}
			case WM_MBUTTONUP:
			{
				Input::m_pInput->OnMouseUp(e_mouseButton::Middle);
				handled = true;
				break;
			}
			case WM_RBUTTONDOWN:
			{
				Input::m_pInput->OnMouseDown(e_mouseButton::Right);
				handled = true;
				break;
			}
			case WM_RBUTTONUP:
			{
				Input::m_pInput->OnMouseUp(e_mouseButton::Right);
				handled = true;
				break;
			}
			}

			if (!handled)
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			timeCur = GetTickCount64();
			Time::m_deltaTime = (timeCur - timeStart) / 1000.0f;

			timeStart = timeCur;

			Update();

			m_pPipeline->DrawingCycle();
			/*m_pPipeline->OpenPipeline();

			Draw(m_pPipeline);

			m_pPipeline->SwitchDrawingTopology();

			LineDraw(m_pPipeline);

			m_pPipeline->ClosePipeline();*/

			Input::m_pInput->Update();
		}
	}

	return msg.wParam;
}

Vector2 Application::GetWindowPosition()
{
	return m_windowPos;
}

Application* Application::GetActiveApplication()
{
	return m_pActiveApplication;
}
Pipeline* Application::GetActivePipeline()
{
	return m_pActiveApplication->m_pPipeline;
}

void Application::SetWindowPosition(const Vector2& a_pos)
{
	m_windowPos = a_pos;

	SetWindowPos(m_window, NULL, (int)m_windowPos.x, (int)m_windowPos.y, m_pPipeline->GetViewWidth(), m_pPipeline->GetViewHeight(), SWP_SHOWWINDOW);
}

HWND & Application::GetWindow()
{
	return m_window;
}

void Application::UpdateWindowPosition()
{
	RECT windowRect;
	GetWindowRect(m_window, &windowRect);
	m_windowPos.x = (float)windowRect.left;
	m_windowPos.y = (float)windowRect.top;
}

void Application::Quit()
{
	PostQuitMessage(0);
}

LRESULT CALLBACK WindowProc(HWND a_hwnd, UINT a_message, WPARAM a_wParam, LPARAM a_lParam)
{
	switch (a_message)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
		break;
	}
	case WM_MOVE:
	case WM_MOVING:
	{
		Application::GetActiveApplication()->UpdateWindowPosition();
		break;
	}
	}

	return DefWindowProc(a_hwnd, a_message, a_wParam, a_lParam);
}
