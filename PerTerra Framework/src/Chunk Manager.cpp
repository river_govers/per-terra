#include "../include/Chunk Manager.h"

#include "../include/Terrain.h"

#include "../include/Application.h"
#include "../include/Pipeline.h"

#include "../include/Maths/Vector3.h"
#include "../include/Maths/Matrix4.h"

#include "../include/Entity.h"

Chunk::Chunk(float a_terrainSize, int a_terrainResolution, Vector2 a_position)
{
	m_pos = a_position;

	m_pTerrain = new Terrain({ a_terrainResolution , a_terrainResolution }, { a_terrainSize, a_terrainSize });
}
Chunk::~Chunk()
{
	if (m_pTerrain)
	{
		delete m_pTerrain;
	}
}

void Chunk::RegisterEntity(Entity* a_entity)
{
	m_entities.push_back(a_entity);
}
void Chunk::RemoveEntity(Entity * a_entity)
{
	m_entities.remove(a_entity);
}

ChunkManager::ChunkManager(int a_chunks, float a_terrainSize, int a_terrainResolution)
{
	m_indicies = a_chunks;
	m_pChunks = new Chunk**[m_indicies];

	m_chunkSize = a_terrainSize;
	m_chunkRes = a_terrainResolution;
	m_chunkScale = a_terrainSize / a_terrainResolution;

	for (int x = 0; x < m_indicies; x++)
	{
		m_pChunks[x] = new Chunk*[m_indicies];

		for (int y = 0; y < m_indicies; y++)
		{
			m_pChunks[x][y] = new Chunk(a_terrainSize, a_terrainResolution, Vector2(x * (m_chunkSize - m_chunkScale), y * (m_chunkSize - m_chunkScale)));
		}
	}
}
ChunkManager::~ChunkManager()
{
	if (m_pChunks)
	{
		for (int x = 0; x < m_indicies; x++)
		{
			if (m_pChunks[x])
			{
				for (int y = 0; y < m_indicies; y++)
				{
					if (m_pChunks[x][y])
					{
						delete m_pChunks[x][y];
					}
				}

				delete m_pChunks[x];
			}
		}

		delete m_pChunks;
	}
}

Chunk& ChunkManager::GetChunk(int a_index)
{
	return *m_pChunks[a_index % m_indicies][a_index / m_indicies];
}
Chunk& ChunkManager::GetChunk(int a_x, int a_y)
{
	return *m_pChunks[a_x][a_y];
}
Chunk& ChunkManager::GetChunk(const Vector2& a_pos)
{
	return *m_pChunks[(int)(a_pos.x / (m_chunkSize - m_chunkScale))][(int)(a_pos.y / (m_chunkSize - m_chunkScale))];
}

Vector2 ChunkManager::GetChunkSize() const
{
	return { m_chunkSize, m_chunkSize };
}

void ChunkManager::Update()
{
	for (int i = 0; i < m_indicies * m_indicies; i++)
	{
		Chunk* chunk = &GetChunk(i);

		for (auto iter = chunk->m_entities.begin(); iter != chunk->m_entities.end(); ++iter)
		{
			if (!(*iter)->m_static)
			{
				(*iter)->m_pTransform->Update();
			}
			
		}
	}
}

void ChunkManager::Draw(int a_indicies, int a_x, int a_y)
{
	Pipeline* pipeline = Application::GetActivePipeline();

	for (int x = ( a_x - (a_indicies / 2)); x <= a_x + a_indicies / 2 && x < m_indicies; ++x)
	{
		if (x < 0)
		{
			continue;
		}

		for (int y = (a_y - (a_indicies / 2)); y <= a_y + a_indicies / 2 && y < m_indicies; ++y)
		{
			if (y < 0)
			{
				continue;
			}

			Vector2 pos = m_pChunks[x][y]->m_pos;

			pipeline->DrawTerrain(Matrix4(1.0f, 0.0f, 0.0f, pos.x,
										  0.0f, 1.0f, 0.0f, 0.0f,
										  0.0f, 0.0f, 1.0f, pos.y,
										  0.0f, 0.0f, 0.0f, 1.0f),
								  *m_pChunks[x][y]->m_pTerrain);

			for (auto iter = m_pChunks[x][y]->m_entities.begin(); iter != m_pChunks[x][y]->m_entities.end(); ++iter)
			{
				(*iter)->Draw();
			}
		}
	}
}
void ChunkManager::Draw(int a_indicies, const Vector2 & a_pos)
{
	Draw(a_indicies, (int)(a_pos.x / (m_chunkSize - m_chunkScale)), (int)(a_pos.y / (m_chunkSize - m_chunkScale)));
}
void ChunkManager::DrawWireFrame(int a_indicies, int a_x, int a_y)
{
	Pipeline* pipeline = Application::GetActivePipeline();

	for (int x = (a_x - (a_indicies / 2)); x <= a_x + a_indicies / 2 && x < m_indicies; x++)
	{
		if (x < 0)
		{
			continue;
		}

		for (int y = (a_y - (a_indicies / 2)); y <= a_y + a_indicies / 2 && y < m_indicies; y++)
		{
			if (y < 0)
			{
				continue;
			}

			Vector2 pos = m_pChunks[x][y]->m_pos;

			pipeline->DrawTerrainWireFrame(Matrix4(1.0f, 0.0f, 0.0f, pos.x, 
												   0.0f, 1.0f, 0.0f, 0.0f, 
												   0.0f, 0.0f, 1.0f, pos.y, 
												   0.0f, 0.0f, 0.0f, 1.0f), 
										   *m_pChunks[x][y]->m_pTerrain,
										   { 0.0f, 1.0f, 0.0f, 1.0f });
		}
	}
}
void ChunkManager::DrawWireFrame(int a_indicies, const Vector2 & a_pos)
{
	DrawWireFrame(a_indicies, (int)(a_pos.x / (m_chunkSize - m_chunkScale)), (int)(a_pos.y / (m_chunkSize - m_chunkScale)));
}
void ChunkManager::DrawDebug(int a_indicies, int a_x, int a_y)
{
	Pipeline* pipeline = Application::GetActivePipeline();

	float size = m_chunkSize - m_chunkScale;

	for (int x = (a_x - (a_indicies / 2)); x <= a_x + (a_indicies / 2) && x <= m_indicies; x++)
	{
		if (x < 0)
		{
			continue;
		}

		for (int y = (a_y - (a_indicies / 2)); y <= a_y + (a_indicies / 2) && y <= m_indicies; y++)
		{
			if (y < 0)
			{
				continue;
			}

			pipeline->DrawLine(Vector3(x * size, -100.0f, y * size), Vector3(x * size, 100.0f, y * size), D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
		}
	}
}
void ChunkManager::DrawDebug(int a_indicies, const Vector2 & a_pos)
{
	DrawDebug(a_indicies, (int)(a_pos.x / (m_chunkSize - m_chunkScale)), (int)(a_pos.y / (m_chunkSize - m_chunkScale)));
}
