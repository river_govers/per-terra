#include "../include/Camera.h"

#include "../include/Application.h"
#include "../include/Pipeline.h"

#include "../include/Maths/Maths.h"

#include <comdef.h>

std::list<Camera*> Camera::m_cameraList = std::list<Camera*>();
Camera* Camera::m_pActiveCamera = nullptr;
Camera* Camera::m_pDrawingCamera = nullptr;

Camera& Camera::GetActiveCamera()
{
	return *m_pActiveCamera;
}
void Camera::SetActiveCamera(Camera& a_camera)
{
	m_pActiveCamera = &a_camera;
}
std::list<Camera*>& Camera::GetCameraList()
{
	return m_cameraList;
}

Camera::Camera(bool a_clearTexture) : Camera(a_clearTexture, Vector2::Zero(), Application::GetActivePipeline()->GetViewDimensions(), 0.0f, 1.0f, DirectX::XM_PIDIV2, {0.1f, 0.1f, 0.1f, 1.0f}, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL)
{
}
Camera::Camera(bool a_clearTexture, float a_fovAngle) : Camera(a_clearTexture, Vector2::Zero(), Application::GetActivePipeline()->GetViewDimensions(), 0.1f, 1.0f, a_fovAngle, {0.1f, 0.1f, 0.1f, 1.0f}, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL)
{
}
Camera::Camera(bool a_clearTexture, float a_fovAngle, const D3DXCOLOR& a_clearColor, UINT a_clearFlags) : Camera(a_clearTexture, Vector2::Zero(), Application::GetActivePipeline()->GetViewDimensions(), 0.0f, 1.0f, a_fovAngle, a_clearColor, a_clearFlags)
{
}
Camera::Camera(bool a_clearTexture, const Vector2& a_viewportStart, const Vector2& a_viewPortSize, float a_fovAngle) : Camera(a_clearTexture, a_viewportStart, a_viewPortSize, 0.0f, 1.0f, a_fovAngle, { 0.1f, 0.1f, 0.1f, 1.0f}, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL)
{
}
Camera::Camera(bool a_clearTexture, const Vector2& a_viewportStart, const Vector2& a_viewPortSize, float a_fovAngle, const D3DXCOLOR & a_clearColor, UINT a_clearFlags) : Camera(a_clearTexture, a_viewportStart, a_viewPortSize, 0.0f, 1.0f, a_fovAngle, a_clearColor, a_clearFlags)
{
}
Camera::Camera(bool a_clearTexture, const Vector2& a_viewportStart, const Vector2& a_viewPortSize, float a_minDepth, float a_maxDepth, float a_fovAngle) : Camera(a_clearTexture, a_viewportStart, a_viewPortSize, 0.0f, 1.0f, a_fovAngle, {0.1f, 0.1f, 0.1f, 1.0f}, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL)
{
}
Camera::Camera(bool a_clearTexture, const Vector2& a_viewportStart, const Vector2& a_viewPortSize, float a_minDepth, float a_maxDepth, float a_fovAngle, const D3DXCOLOR& a_clearColor, UINT a_clearFlags)
{
	using namespace DirectX;

	m_clearTexture = a_clearTexture;

	m_cameraList.push_back(this);

	if (!m_pActiveCamera)
	{
		m_pActiveCamera = this;
	}

	Pipeline* pipeline = Application::GetActivePipeline();

	Vector2 size = pipeline->GetViewDimensions();

	m_projection = XMMatrixPerspectiveFovLH(a_fovAngle, a_viewPortSize.x / a_viewPortSize.y, 0.01f, 100.0f);

	m_viewport.Width = a_viewPortSize.x;
	m_viewport.Height = a_viewPortSize.y;
	m_viewport.TopLeftX = a_viewportStart.x;
	m_viewport.TopLeftY = a_viewportStart.y;
	m_viewport.MinDepth = a_minDepth;
	m_viewport.MaxDepth = a_maxDepth;

	m_clearColor = a_clearColor;
	m_clearFlags = a_clearFlags;
}
Camera::~Camera()
{
	for (auto iter = m_cameraList.begin(); iter != m_cameraList.end(); iter++)
	{
		if (*iter == this)
		{
			m_cameraList.erase(iter);

			break;
		}
	}

	if (m_pActiveCamera == this)
	{
		if (!m_cameraList.empty())
		{
			m_pActiveCamera = *m_cameraList.begin();
		}
		else
		{
			m_pActiveCamera = nullptr;
		}
	}

	if (m_renderTarget)
	{
		m_renderTarget->Release();
	}
}

Transform& Camera::Transform()
{
	return m_transform;
}

Matrix4 Camera::DrawMatrix() const
{
	return m_view * m_projection;
}
Matrix4 Camera::ViewMatrix() const
{
	return m_view;
}

Vector3 Camera::ScreenToWorld(const Vector3& a_position) const
{
	using namespace DirectX;

	Vector2 viewDimensions = Application::GetActivePipeline()->GetViewDimensions();

 	//Matrix4 matrix = (m_projection * m_view).Inversed();
	Matrix4 matrix = XMMatrixInverse(nullptr, (m_projection * m_view).ToXMMatrix());

	Vector3 viewPort = { a_position.x / viewDimensions.x, a_position.y / viewDimensions.y, a_position.z };

	Vector3 dir = matrix * viewPort;

	float w = 1.0f / (viewPort.x * matrix.ind_30 + viewPort.y * matrix.ind_31 + viewPort.z * matrix.ind_32 + matrix.ind_33);

	dir *= w;

	return dir;
}
