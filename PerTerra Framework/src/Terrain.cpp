#include "../include/Terrain.h"

#include "../include/Application.h"
#include "../include/Pipeline.h"

#include <vector>

// TODO: Work out drawing bug with large gap between x and y resolution
Terrain::Terrain(const DirectX::XMINT2& a_resolution, const Vector2& a_size)
{
	using namespace DirectX;

#pragma region Init
	m_resolution = a_resolution;
	m_size = a_size;

	UINT size = m_resolution.x * m_resolution.y;
	
	m_pVertCache = new Vertex[size];
	
	int x = 0;
	int z = 0;

	XMFLOAT2 scale = { m_size.x / m_resolution.x, m_size.y / m_resolution.y };

	for (UINT i = 0; i < size; i++)
	{
		if (z >= m_resolution.x)
		{
			z = 0;
			x++;
		}

		m_pVertCache[i] = { { x * scale.x, 0.0f, z * scale.y }, { 0.0f, 1.0f, 0.0f } };

		z++;
	}

	std::vector<UINT> indicies;

	UINT indSize = ((m_resolution.x - 1) * (m_resolution.y - 1)) * 6;

	indicies.reserve(indSize);
	
	x = 0;

	for (UINT i = 0; indicies.size() < indSize; i++)
	{
		if (x >= m_resolution.y - 1)
		{
			x = 0;
			i++;
		}

		indicies.push_back(i);
		indicies.push_back(i + 1);
		indicies.push_back(i + m_resolution.y);

		indicies.push_back(i + 1);
		indicies.push_back(i + m_resolution.y + 1);
		indicies.push_back(i + m_resolution.y);

		x++;
	}

#pragma endregion

	m_indexCount = indicies.size();

#pragma region VerticeBufferInit
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(Vertex) * (size);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	D3D11_MAPPED_SUBRESOURCE ms;
	ZeroMemory(&ms, sizeof(ms));

	HRESULT error;

	error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &m_pVertexBuffer);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to create vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}

	error = Application::GetActivePipeline()->m_pDeviceContext->Map(m_pVertexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to open vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
	memcpy(ms.pData, m_pVertCache, (size) * sizeof(Vertex));
	Application::GetActivePipeline()->m_pDeviceContext->Unmap(m_pVertexBuffer, NULL);
#pragma endregion

#pragma region IndiceBufferInit
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(UINT) * indicies.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	ZeroMemory(&ms, sizeof(ms));

	error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &m_pIndiceBuffer);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to create index buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}

	error = Application::GetActivePipeline()->m_pDeviceContext->Map(m_pIndiceBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to open index buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
	memcpy(ms.pData, &indicies[0], indicies.size() * sizeof(UINT));
	Application::GetActivePipeline()->m_pDeviceContext->Unmap(m_pIndiceBuffer, NULL);
#pragma endregion
}
Terrain::~Terrain()
{
	delete[] m_pVertCache;
}

void Terrain::UpdateBuffer()
{
	int size = m_resolution.x * m_resolution.y;

	D3D11_MAPPED_SUBRESOURCE ms;
	ZeroMemory(&ms, sizeof(ms));

	HRESULT error = Application::GetActivePipeline()->m_pDeviceContext->Map(m_pVertexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to open vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
	memcpy(ms.pData, m_pVertCache, size * sizeof(Vertex));
	Application::GetActivePipeline()->m_pDeviceContext->Unmap(m_pVertexBuffer, NULL);
}

Vertex& Terrain::GetIndex(int a_x, int a_y)
{
	return m_pVertCache[a_x * m_resolution.y + a_y];
}

DirectX::XMINT2 Terrain::GetVerticieCount()
{
	return m_resolution;
}
Vector2 Terrain::GetSize()
{
	return m_size;
}

Vector2 Terrain::GetSpacing()
{
	return { m_size.x / m_resolution.x, m_size.y / m_resolution.y };
}
