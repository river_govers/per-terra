#include "..\include\Transform.h"
#include "..\include\Maths\Maths.h"

#include "..\include\Chunk Manager.h"

// Use namespace in functions causes namespace pollution otherwise

Vector3 Transform::WorldForward() const
{
	if (m_parent)
	{
		return (m_parent->WorldForward() + m_forward).NormalizedInv();
	}

	return m_forward;
}
Vector3 Transform::WorldUp() const
{
	if (m_parent)
	{
		return (m_parent->WorldUp() + m_up).NormalizedInv();
	}

	return m_up;
}
Vector3 Transform::WorldRight() const
{
	if (m_parent)
	{
		return (m_parent->WorldRight() + m_up).NormalizedInv();
	}

	return m_right;
}

void Transform::Update()
{
	m_chunk->RemoveEntity(m_entity);

	Vector3 worldPos = GetPosition();
	m_chunk = &m_chunkManager->GetChunk({ worldPos.x, worldPos.z });
	m_chunk->RegisterEntity(m_entity);
}

Transform::Transform() : m_translation(), m_rotation(), m_forward(Vector3::Forward()), m_up(Vector3::Up()), m_right(Vector3::Right()), m_scale(1.0f, 1.0f, 1.0f), 
						 m_parent(nullptr), m_entity(nullptr), m_chunkManager(nullptr), m_chunk(nullptr)
{
}

Transform::Transform(Entity* a_entity, ChunkManager* a_chunkManager) : m_translation(), m_rotation(), m_forward(Vector3::Forward()), m_up(Vector3::Up()), m_right(Vector3::Right()), m_scale(1.0f, 1.0f, 1.0f), 
																	   m_parent(nullptr), m_entity(a_entity), m_chunkManager(a_chunkManager)
{
	m_chunk = &a_chunkManager->GetChunk({ m_translation.x, m_translation.z });
	m_chunk->RegisterEntity(a_entity);
}
Transform::~Transform()
{
	if (m_entity)
	{
		m_chunk->RemoveEntity(m_entity);
	}
}

Matrix4 Transform::ToMatrix() const
{
	 Matrix4 matrix = { m_scale.x, 0.0f,	  0.0f,		 0.0f,
						0.0f,	   m_scale.y, 0.0f,		 0.0f,
						0.0f,	   0.0f,	  m_scale.z, 0.0f,
						0.0f,	   0.0f,	  0.0f,		 1.0f };
	matrix *= { 1.0f, 0.0f, 0.0f, m_translation.x,
				0.0f, 1.0f, 0.0f, m_translation.y,
				0.0f, 0.0f, 1.0f, m_translation.z,
				0.0f, 0.0f, 0.0f, 1.0f };
	matrix *= { m_right.x, m_up.x, m_forward.x, 0.0f,
				m_right.y, m_up.y, m_forward.y, 0.0f,
				m_right.z, m_up.z, m_forward.z, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f };

	if (m_parent)
	{
		return matrix * m_parent->ToMatrix();
	}

	return matrix;
}

Transform::operator Matrix4()
{
	return ToMatrix();
}

void Transform::SetParent(Transform & a_parent)
{
	m_parent = &a_parent;
}

void Transform::SetMatrix(const Matrix4& a_matix)
{
	using namespace DirectX;

	XMVECTOR translation;
	XMVECTOR scale;
	XMVECTOR rotation;

	XMMatrixDecompose(&scale, &rotation, &translation, a_matix.ToXMMatrix());

	XMMATRIX rotationMatrix = XMMatrixRotationQuaternion(rotation);

	m_right = rotationMatrix.r[0];
	m_up = rotationMatrix.r[1];
	m_forward = rotationMatrix.r[2];

	m_rotation = QuaternionToEuler(rotation);

	m_translation = translation;
	m_scale = scale;
}

Matrix4 Transform::operator =(const Matrix4& a_matrix)
{
	SetMatrix(a_matrix);

	return ToMatrix();
}

Matrix4 Transform::operator *(const Matrix4& a_matrix) const
{
	return a_matrix * ToMatrix();
}
Matrix4 Transform::operator *=(const Matrix4& a_matrix)
{
	return *this = a_matrix * ToMatrix();
}

Vector3 Transform::GetLocalRotation() const
{
	return m_rotation;
}
Vector3 Transform::GetRotation() const
{
	if (m_parent)
	{
		return m_parent->GetRotation() + m_rotation;
	}

	return m_rotation;
}

Vector3 Transform::GetLocalPosition() const
{
	return m_translation;
}
Vector3 Transform::GetPosition() const
{
	if (m_parent)
	{
		Vector3 scale = GetScale();
		Vector3 forward = WorldForward();
		Vector3 worldRight = WorldRight();

		return m_parent->GetPosition() + (scale.x * (m_right * m_translation.x) + scale.y * (m_up * m_translation.y) + scale.z * (m_forward * m_translation.z));
	}

	return m_scale.x * (m_right * m_translation.x) + m_scale.y * (m_up * m_translation.y) + m_scale.z * (m_forward * m_translation.z);
}

Vector3 Transform::GetLocalScale() const
{
	return m_scale;
}
Vector3 Transform::GetScale() const
{
	if (m_parent)
	{
		Vector3 parentScale = m_parent->GetScale();

		return { m_scale.x * parentScale.x, m_scale.y * parentScale.y, m_scale.z * parentScale.z };
	}

	return m_scale;
}

void Transform::SetLocalScale(const Vector3& a_scale)
{
	m_scale = a_scale;
}

void Transform::SetLocalRotation(const Vector3& a_euler)
{
	m_rotation = a_euler;

	m_up = { sinf(a_euler.z), cosf(a_euler.z), 0.0f };

	m_forward = { sinf(a_euler.y) * cosf(a_euler.x), -sinf(a_euler.x), cosf(a_euler.y) * cosf(a_euler.x) };

	m_right = Vector3::Cross(m_up, m_forward);
	m_up = Vector3::Cross(m_forward, m_right);

	m_forward.NormalizeInv();
	m_up.NormalizeInv();
	m_right.NormalizeInv();
}

void Transform::Rotate(const Vector3& a_euler)
{
	SetLocalRotation(m_rotation + a_euler);
}

void Transform::Translate(const Vector3& a_translation)
{
	m_translation = { m_translation.x + a_translation.x, m_translation.y + a_translation.y, m_translation.z + a_translation.z };
}
void Transform::SetLocalPosition(const Vector3& a_position)
{
	m_translation = a_position;
}

Vector3 Transform::Forward() const
{
	return m_forward;
}
Vector3 Transform::Up() const
{
	return m_up;
}
Vector3 Transform::Right() const
{
	return m_right;
}
