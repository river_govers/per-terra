#include "..\include\Pipeline.h"
#include "..\include\Application.h"

#include <Windows.h>

#include <d3dcompiler.h>

#pragma warning (disable : 4005)

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")

#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBGUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBGUG_NEW
#endif 

#include "../include/Model.h"
#include "../include/Terrain.h"

#include <comdef.h>
#include "../include/Camera.h"

bool Pipeline::InitD3D(const HWND& a_window)
{
	HRESULT hr;

#pragma region Initialise DirectX
	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	DXGI_SWAP_CHAIN_DESC scd;

	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));

	scd.BufferCount = 1;
	scd.BufferDesc.Width = (UINT)m_screenDimension.x;
	scd.BufferDesc.Height = (UINT)m_screenDimension.y;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.RefreshRate.Numerator = 60;
	scd.BufferDesc.RefreshRate.Denominator = 1;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.OutputWindow = a_window;
	scd.SampleDesc.Count = 1;
	scd.SampleDesc.Quality = 0;
	scd.Windowed = TRUE;
	scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	hr = D3D11CreateDeviceAndSwapChain(NULL,
									   D3D_DRIVER_TYPE_HARDWARE,
									   NULL,
									   createDeviceFlags,
									   NULL,
									   NULL,
									   D3D11_SDK_VERSION,
									   &scd,
									   &m_pSwapChain,
									   &m_pDevice,
									   NULL,
									   &m_pDeviceContext);

	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Failed to create device: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}
#pragma endregion

#pragma region Create Raterizers
	D3D11_RASTERIZER_DESC rd;
	ZeroMemory(&rd, sizeof(rd));

	rd.FillMode = D3D11_FILL_SOLID;
	rd.CullMode = D3D11_CULL_BACK;
	rd.FrontCounterClockwise = FALSE;
	rd.DepthBias = 0;
	rd.SlopeScaledDepthBias = 0.0f; 
	rd.DepthBiasClamp = 0.0f;
	rd.DepthClipEnable = TRUE;
	rd.ScissorEnable = FALSE;
	rd.MultisampleEnable = FALSE;
	rd.AntialiasedLineEnable = FALSE;

	hr = m_pDevice->CreateRasterizerState(&rd, &m_pSolidRasterizerState);

	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Failed to create raterizer state: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}

	ZeroMemory(&rd, sizeof(rd));

	rd.FillMode = D3D11_FILL_WIREFRAME;
	rd.CullMode = D3D11_CULL_BACK;
	rd.FrontCounterClockwise = FALSE;
	rd.DepthBias = 0;
	rd.SlopeScaledDepthBias = 0.0f;
	rd.DepthBiasClamp = 0.0f;
	rd.DepthClipEnable = TRUE;
	rd.ScissorEnable = FALSE;
	rd.MultisampleEnable = FALSE;
	rd.AntialiasedLineEnable = FALSE;

	hr = m_pDevice->CreateRasterizerState(&rd, &m_pWireRasterizerState);

	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Failed to create raterizer state: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}

	m_pDeviceContext->RSSetState(m_pSolidRasterizerState);
#pragma endregion

#pragma region Set the render target
	ID3D11Texture2D* pBackBuffer;
	hr = m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

	if (FAILED(hr))
	{
		MessageBox(NULL, "Buffer initialisation failed", "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}

	hr = m_pDevice->CreateRenderTargetView(pBackBuffer, NULL, &m_pBackBuffer);
	pBackBuffer->Release();
	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Viewport initialisation failed: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);

		return false;
	}
#pragma endregion

#pragma region Create the Depth Stencil
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));

	descDepth.Width = (UINT)m_screenDimension.x;
	descDepth.Height = (UINT)m_screenDimension.y;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;

	hr = m_pDevice->CreateTexture2D(&descDepth, nullptr, &m_pDepthStencil);

	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Depth stencil initialisation failed: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);

		return false;
	}

	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));

	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	hr = m_pDevice->CreateDepthStencilView(m_pDepthStencil, &descDSV, &m_pDepthStencilView);

	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Depth stencil m_view initialisation failed: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);

		return false;
	}
#pragma endregion

	return true;
}
bool Pipeline::InitPipeline()
{
#pragma region InitShaders
	ID3D10Blob* VS, *PS, *SPS;

	DWORD shaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#ifdef _DEBUG
	shaderFlags |= D3DCOMPILE_DEBUG;

	shaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif // _DEBUG

	ID3DBlob* pErrorBlob = nullptr;

	if (FAILED(D3DX11CompileFromFile("shaders.hlsl", 0, 0, "VShader", "vs_4_0", shaderFlags, 0, 0, &VS, &pErrorBlob, 0)))
	{
		if (pErrorBlob)
		{
			OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
			pErrorBlob->Release();
		}

		MessageBox(NULL, "Vertex shader failed to load", "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}

	if (pErrorBlob)
	{
		pErrorBlob->Release();
	}

	if (FAILED(D3DX11CompileFromFile("shaders.hlsl", 0, 0, "PShader", "ps_4_0", shaderFlags, 0, 0, &PS, &pErrorBlob, 0)))
	{
		if (pErrorBlob)
		{
			OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
			pErrorBlob->Release();
		}

		MessageBox(NULL, "Pixel shader failed to load", "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}

	if (pErrorBlob)
	{
		pErrorBlob->Release();
	}

	if (FAILED(D3DX11CompileFromFile("shaders.hlsl", 0, 0, "PShaderSolid", "ps_4_0", shaderFlags, 0, 0, &SPS, &pErrorBlob, 0)))
	{
		if (pErrorBlob)
		{
			OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
			pErrorBlob->Release();
		}

		MessageBox(NULL, "Solid pixel shader failed to load", "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}

	if (pErrorBlob)
	{
		pErrorBlob->Release();
	}

	m_pDevice->CreateVertexShader(VS->GetBufferPointer(), VS->GetBufferSize(), NULL, &m_pVertexShader);
	m_pDevice->CreatePixelShader(PS->GetBufferPointer(), PS->GetBufferSize(), NULL, &m_pPixelShader);
	m_pDevice->CreatePixelShader(SPS->GetBufferPointer(), SPS->GetBufferSize(), NULL, &m_pSolidPixelShader);

	m_pDeviceContext->VSSetShader(m_pVertexShader, 0, 0);
	m_pDeviceContext->PSSetShader(m_pPixelShader, 0, 0);
#pragma endregion

#pragma region InitMemoryLayout
	D3D11_INPUT_ELEMENT_DESC layout[] = { { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
										  { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 } };

	if (FAILED(m_pDevice->CreateInputLayout(layout, ARRAYSIZE(layout), VS->GetBufferPointer(), VS->GetBufferSize(), &m_pLayout)))
	{
		MessageBox(NULL, "Failed to initialise memory layout", "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}

	m_pDeviceContext->IASetInputLayout(m_pLayout);

	VS->Release();
	PS->Release();
	SPS->Release();
#pragma endregion

	return true;
}
bool Pipeline::InitGraphics()
{
	HRESULT hr;
#pragma region LineInit
#pragma region LineVert
	Vertex line[] =
	{
		{{0.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 1.0f}},
		{{0.0f, 0.0f, 1.0f}, {1.0f, 1.0f, 1.0f}}
	};
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(Vertex) * 2;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	D3D11_MAPPED_SUBRESOURCE ms;
	ZeroMemory(&ms, sizeof(ms));

	hr = m_pDevice->CreateBuffer(&bd, NULL, &m_pLineBuffer);
	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Failed to create vertex buffer: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}

	hr = m_pDeviceContext->Map(m_pLineBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Failed to open vertex buffer: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}
	memcpy(ms.pData, line, sizeof(line));

	m_pDeviceContext->Unmap(m_pLineBuffer, NULL);
#pragma endregion

#pragma region LineInd
	WORD indices[] =
	{
		0,
		1
	};

	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(WORD) * 2;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	ZeroMemory(&ms, sizeof(ms));

	hr = m_pDevice->CreateBuffer(&bd, NULL, &m_pLineIndBuffer);
	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Failed to create indice buffer: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}

	hr = m_pDeviceContext->Map(m_pLineIndBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Failed to open indice buffer: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}
	memcpy(ms.pData, indices, sizeof(indices));

	m_pDeviceContext->Unmap(m_pLineIndBuffer, NULL);
#pragma endregion
#pragma endregion

#pragma region ConstantBufferInit
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	if (FAILED(m_pDevice->CreateBuffer(&bd, nullptr, &m_pConstantBuffer)))
	{
		MessageBox(NULL, "Failed to create constant buffer", "DirectX Error", MB_OK | MB_ICONERROR);
		return false;
	}
#pragma endregion
	return true;
}

Pipeline::Pipeline(const HWND& a_window, int a_screenWidth, int a_screenHeight)
{
	m_screenDimension.x = (float)a_screenWidth;
	m_screenDimension.y = (float)a_screenHeight;

	m_success = InitD3D(a_window);
	if (m_success)
	{
		m_success = InitPipeline();
		if (m_success)
		{
			m_success = InitGraphics();
		}
	}
}
Pipeline::~Pipeline()
{
	m_pSwapChain->SetFullscreenState(FALSE, NULL);

	for (auto iter = Camera::GetCameraList().begin(); iter != Camera::GetCameraList().end(); )
	{
		if (*iter)
		{ 
			delete *iter;
 
			iter = Camera::GetCameraList().begin();
		}
		else
		{
			iter++;
		}
	}

	Camera::GetCameraList().clear();

	if (m_pSwapChain)
	{
		m_pSwapChain->Release();
	}
	if (m_pDeviceContext)
	{
		m_pDeviceContext->ClearState();
	}

	if (m_pBackBuffer)
	{
		m_pBackBuffer->Release();
	}
	if (m_pDepthStencil)
	{
		m_pDepthStencil->Release();
	}
	if (m_pDepthStencilView)
	{
		m_pDepthStencilView->Release();
	}
	if (m_pConstantBuffer)
	{
		m_pConstantBuffer->Release();
	}
	if (m_pLayout)
	{
		m_pLayout->Release();
	}

	if (m_pSolidRasterizerState)
	{
		m_pSolidRasterizerState->Release();
	}
	if (m_pWireRasterizerState)
	{
		m_pWireRasterizerState->Release();
	}

	if (m_pLineBuffer)
	{
		m_pLineBuffer->Release();
	}
	if (m_pLineIndBuffer)
	{
		m_pLineIndBuffer->Release();
	}

	if (m_pPixelShader)
	{
		m_pPixelShader->Release();
	}
	if (m_pVertexShader)
	{
		m_pVertexShader->Release();
	}
	if (m_pSolidPixelShader)
	{
		m_pSolidPixelShader->Release();
	}

	if (m_pDeviceContext)
	{
		m_pDeviceContext->Release();
	}
#ifdef _DEBUG
	if (m_pDevice)
	{
		m_pDevice->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&m_d3dDebug));

		if (m_d3dDebug)
		{
			m_d3dDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);

			m_d3dDebug->Release();
		}
		m_pDevice->Release();
	}
#endif // _DEBUG
}

void Pipeline::DrawingCycle()
{
	for (auto iter = Camera::GetCameraList().begin(); iter != Camera::GetCameraList().end(); iter++)
	{
		//(*iter)->m_view = (*iter)->Transform().ToMatrix().Inversed();
		(*iter)->m_view = XMMatrixInverse(nullptr, (*iter)->Transform().ToMatrix().ToXMMatrix());

		Camera::m_pDrawingCamera = (*iter);

		OpenPipeline();
		
		Application::GetActiveApplication()->Draw(this);

		SwitchDrawingTopology();

		Application::GetActiveApplication()->LineDraw(this);	
	}

	ClosePipeline();
}

void Pipeline::OpenPipeline()
{
	m_pDeviceContext->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	Camera* camera = Camera::m_pDrawingCamera;

	m_pDeviceContext->RSSetViewports(1, &camera->m_viewport);

	// Clear the texture
	if (camera->m_renderTarget)
	{
		m_pDeviceContext->OMSetRenderTargets(1, &camera->m_renderTarget, m_pDepthStencilView);
		if (camera->m_clearTexture)
		{
			m_pDeviceContext->ClearRenderTargetView(camera->m_renderTarget, camera->m_clearColor);
		}
	}
	else
	{
		m_pDeviceContext->OMSetRenderTargets(1, &m_pBackBuffer, m_pDepthStencilView);
		if (camera->m_clearTexture)
		{
			m_pDeviceContext->ClearRenderTargetView(m_pBackBuffer, camera->m_clearColor);
		}
	}

	m_pDeviceContext->ClearDepthStencilView(m_pDepthStencilView, camera->m_clearFlags, 1.0f, 0);

	// Reset the shaders and buffers
	m_pDeviceContext->VSSetShader(m_pVertexShader, nullptr, 0);
	m_pDeviceContext->VSSetConstantBuffers(0, 1, &m_pConstantBuffer);
	m_pDeviceContext->PSSetShader(m_pPixelShader, nullptr, 0);
	m_pDeviceContext->PSSetConstantBuffers(0, 1, &m_pConstantBuffer);
}
void Pipeline::SwitchDrawingTopology()
{
	m_pDeviceContext->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINELIST);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	m_pDeviceContext->IASetVertexBuffers(0, 1, &m_pLineBuffer, &stride, &offset);
	m_pDeviceContext->IASetIndexBuffer(m_pLineIndBuffer, DXGI_FORMAT_R16_UINT, 0);

	m_pDeviceContext->PSSetShader(m_pSolidPixelShader, nullptr, 0);
}
void Pipeline::ClosePipeline()
{
	// Swaps the buffer
	m_pSwapChain->Present(1, 0);
}

int Pipeline::GetViewWidth() const
{
	return (int)m_screenDimension.x;
}
int Pipeline::GetViewHeight() const
{
	return (int)m_screenDimension.y;
}
Vector2 Pipeline::GetViewDimensions() const
{
	return m_screenDimension;
}

void Pipeline::DrawModel(const Matrix4& a_transform, const Model& a_model)
{
	DrawModel(a_transform.ToXMMatrix(), a_model);
}
void Pipeline::DrawModelWireFrame(const Matrix4& a_transform, const Model& a_model, const D3DXCOLOR& a_color)
{
	DrawModelWireFrame(a_transform.ToXMMatrix(), a_model, a_color);
}
void Pipeline::DrawTerrain(const Matrix4& a_transform, const Terrain& a_terrain)
{
	DrawTerrain(a_transform.ToXMMatrix(), a_terrain);
}
void Pipeline::DrawTerrainWireFrame(const Matrix4& a_transform, const Terrain & a_terrain, const D3DXCOLOR & a_color)
{
	DrawTerrainWireFrame(a_transform.ToXMMatrix(), a_terrain, a_color);
}

void Pipeline::DrawModel(const DirectX::XMMATRIX& a_transform, const Model& a_model)
{
	using namespace DirectX;

	// TODO: Clean up the buffers
	XMFLOAT4 vLightDirs[2] =
	{
		XMFLOAT4(0.98058f, -0.19612f, 0.0f, 0.0f),
		XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f),
	};
	D3DXCOLOR vLightColors[2] =
	{
		D3DXCOLOR(0.88235f, 0.70588f, 0.70588f, 1.0f),
		D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f)
	};

	ConstantBuffer cb;

	cb.world = XMMatrixTranspose(a_transform);
	cb.m_view = Camera::m_pDrawingCamera->m_view.ToXMMatrix(false);
	cb.m_projection = Camera::m_pDrawingCamera->m_projection.ToXMMatrix(false);
	cb.lightColors[0] = vLightColors[0];
	cb.lightColors[1] = vLightColors[1];
	cb.lightDirs[0] = vLightDirs[0];
	cb.lightDirs[1] = vLightDirs[1];

	m_pDeviceContext->UpdateSubresource(m_pConstantBuffer, 0, nullptr, &cb, 0, 0);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	m_pDeviceContext->IASetVertexBuffers(0, 1, &a_model.m_pVertexBuffer, &stride, &offset);
	m_pDeviceContext->IASetIndexBuffer(a_model.m_pIndiceBuffer, DXGI_FORMAT_R16_UINT, 0);

	if (a_model.m_pPixelShader)
	{
		m_pDeviceContext->PSSetShader(a_model.m_pPixelShader, nullptr, 0);
	}
	if (a_model.m_pVertexShader)
	{
		m_pDeviceContext->VSSetShader(a_model.m_pVertexShader, nullptr, 0);
	}

	m_pDeviceContext->DrawIndexed(a_model.m_indexCount, 0, 0);

	if (a_model.m_pPixelShader)
	{
		m_pDeviceContext->PSSetShader(m_pPixelShader, nullptr, 0);
	}
	if (a_model.m_pVertexShader)
	{
		m_pDeviceContext->VSSetShader(m_pVertexShader, nullptr, 0);
	}
}
void Pipeline::DrawModelWireFrame(const DirectX::XMMATRIX & a_transform, const Model & a_model, const D3DXCOLOR& a_color)
{
	using namespace DirectX;

	m_pDeviceContext->RSSetState(m_pWireRasterizerState);
	
	ConstantBuffer cb;

	cb.world = XMMatrixTranspose(a_transform);
	//cb.m_view = XMMatrixTranspose((XMMATRIX)Camera::m_pDrawingCamera->m_view);
	//cb.m_projection = XMMatrixTranspose((XMMATRIX)Camera::m_pDrawingCamera->m_projection);
	cb.m_view = Camera::m_pDrawingCamera->m_view.ToXMMatrix(false);
	cb.m_projection = Camera::m_pDrawingCamera->m_projection.ToXMMatrix(false);
	cb.outColor = a_color;

	m_pDeviceContext->UpdateSubresource(m_pConstantBuffer, 0, nullptr, &cb, 0, 0);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	m_pDeviceContext->IASetVertexBuffers(0, 1, &a_model.m_pVertexBuffer, &stride, &offset);
	m_pDeviceContext->IASetIndexBuffer(a_model.m_pIndiceBuffer, DXGI_FORMAT_R16_UINT, 0);

	m_pDeviceContext->PSSetShader(m_pSolidPixelShader, nullptr, 0);
	if (a_model.m_pVertexShader)
	{
		m_pDeviceContext->VSSetShader(a_model.m_pVertexShader, nullptr, 0);
	}

	m_pDeviceContext->DrawIndexed(a_model.m_indexCount, 0, 0);

	m_pDeviceContext->PSSetShader(m_pPixelShader, nullptr, 0);
	if (a_model.m_pVertexShader)
	{
		m_pDeviceContext->VSSetShader(m_pVertexShader, nullptr, 0);
	}

	m_pDeviceContext->RSSetState(m_pSolidRasterizerState);
}
void Pipeline::DrawTerrain(const DirectX::XMMATRIX& a_transform, const Terrain & a_terrain)
{
	using namespace DirectX;

	XMFLOAT4 vLightDirs[2] =
	{
		XMFLOAT4(0.98058f, -0.19612f, 0.0f, 0.0f),
		XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f),
	};
	D3DXCOLOR vLightColors[2] =
	{
		D3DXCOLOR(0.88235f, 0.70588f, 0.70588f, 1.0f),
		D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f)
	};

	ConstantBuffer cb;

	cb.world = XMMatrixTranspose(a_transform);
	cb.m_view = Camera::m_pDrawingCamera->m_view.ToXMMatrix(false);
	cb.m_projection = Camera::m_pDrawingCamera->m_projection.ToXMMatrix(false);
	cb.lightColors[0] = vLightColors[0];
	cb.lightColors[1] = vLightColors[1];
	cb.lightDirs[0] = vLightDirs[0];
	cb.lightDirs[1] = vLightDirs[1];

	m_pDeviceContext->UpdateSubresource(m_pConstantBuffer, 0, nullptr, &cb, 0, 0);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	m_pDeviceContext->IASetVertexBuffers(0, 1, &a_terrain.m_pVertexBuffer, &stride, &offset);
	m_pDeviceContext->IASetIndexBuffer(a_terrain.m_pIndiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	if (a_terrain.m_pPixelShader)
	{
		m_pDeviceContext->PSSetShader(a_terrain.m_pPixelShader, nullptr, 0);
	}
	if (a_terrain.m_pVertexShader)
	{
		m_pDeviceContext->VSSetShader(a_terrain.m_pVertexShader, nullptr, 0);
	}

	m_pDeviceContext->DrawIndexed(a_terrain.m_indexCount, 0, 0);

	if (a_terrain.m_pPixelShader)
	{
		m_pDeviceContext->PSSetShader(m_pPixelShader, nullptr, 0);
	}
	if (a_terrain.m_pVertexShader)
	{
		m_pDeviceContext->VSSetShader(m_pVertexShader, nullptr, 0);
	}
}
void Pipeline::DrawTerrainWireFrame(const DirectX::XMMATRIX & a_transform, const Terrain & a_terrain, const D3DXCOLOR& a_color)
{
	using namespace DirectX;

	m_pDeviceContext->RSSetState(m_pWireRasterizerState);

	ConstantBuffer cb;

	cb.world = XMMatrixTranspose(a_transform);
	cb.m_view = Camera::m_pDrawingCamera->m_view.ToXMMatrix(false);
	cb.m_projection = Camera::m_pDrawingCamera->m_projection.ToXMMatrix(false);
	cb.outColor = a_color;

	m_pDeviceContext->UpdateSubresource(m_pConstantBuffer, 0, nullptr, &cb, 0, 0);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	m_pDeviceContext->IASetVertexBuffers(0, 1, &a_terrain.m_pVertexBuffer, &stride, &offset);
	m_pDeviceContext->IASetIndexBuffer(a_terrain.m_pIndiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	m_pDeviceContext->PSSetShader(m_pSolidPixelShader, nullptr, 0);
	if (a_terrain.m_pVertexShader)
	{
		m_pDeviceContext->VSSetShader(a_terrain.m_pVertexShader, nullptr, 0);
	}

	m_pDeviceContext->DrawIndexed(a_terrain.m_indexCount, 0, 0);

	m_pDeviceContext->PSSetShader(m_pPixelShader, nullptr, 0);
	if (a_terrain.m_pVertexShader)
	{
		m_pDeviceContext->VSSetShader(m_pVertexShader, nullptr, 0);
	}

	m_pDeviceContext->RSSetState(m_pSolidRasterizerState);
}

void Pipeline::DrawLine(const DirectX::XMFLOAT3& a_startPos, const DirectX::XMFLOAT3& a_endPos, const D3DXCOLOR& a_color)
{
	using namespace DirectX;

	if (a_startPos.x == a_endPos.x && a_startPos.y == a_endPos.y && a_startPos.z == a_endPos.z)
	{
		return;
	}

	XMFLOAT3 vecBetween = { a_endPos.x - a_startPos.x, a_endPos.y - a_startPos.y, a_endPos.z - a_startPos.z };
	float magnitude = sqrt(vecBetween.x * vecBetween.x + vecBetween.y * vecBetween.y + vecBetween.z * vecBetween.z);

	XMMATRIX matrix = XMMatrixIdentity();

	XMVECTOR upVec = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	XMVECTOR normDir = XMVector4Normalize(XMVectorSet(vecBetween.x, vecBetween.y, vecBetween.z, 0.0f));

	XMVECTOR dot = XMVector4Dot(normDir, upVec);

	if (abs(XMVectorGetW(dot)) == 1.0f)
	{
		upVec = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
	}

	XMVECTOR right = XMVector3Cross(upVec, normDir);
	upVec = XMVector3Cross(normDir, right);

	normDir = XMVectorScale(normDir, magnitude);

	matrix = XMMatrixSet(XMVectorGetByIndex(right, 0), XMVectorGetByIndex(right, 1), XMVectorGetByIndex(right, 2), XMVectorGetByIndex(right, 3),
						 XMVectorGetByIndex(upVec, 0), XMVectorGetByIndex(upVec, 1), XMVectorGetByIndex(upVec, 2), XMVectorGetByIndex(upVec, 3),
						 XMVectorGetByIndex(normDir, 0), XMVectorGetByIndex(normDir, 1), XMVectorGetByIndex(normDir, 2), XMVectorGetByIndex(normDir, 3),
						 a_startPos.x, a_startPos.y, a_startPos.z, 1.0f);

	ConstantBuffer cb;

	cb.world = XMMatrixTranspose(matrix);
	cb.m_view = Camera::m_pDrawingCamera->m_view.ToXMMatrix(false);
	cb.m_projection = Camera::m_pDrawingCamera->m_projection.ToXMMatrix(false);
	cb.outColor = a_color;

	m_pDeviceContext->UpdateSubresource(m_pConstantBuffer, 0, nullptr, &cb, 0, 0);

	m_pDeviceContext->DrawIndexed(2, 0, 0);
}
void Pipeline::DrawLine(const Vector3& a_startPos, const Vector3& a_endPos, const D3DXCOLOR& a_color)
{
	using namespace DirectX;

	DrawLine((XMFLOAT3)a_startPos, (XMFLOAT3)a_endPos, a_color);
}
