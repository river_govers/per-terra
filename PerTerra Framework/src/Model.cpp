#include "../include/Model.h"

//#include "../include/Texture.h"

#include "../include/Application.h"
#include "../include/Pipeline.h"

#include <vector>

#include <fstream>
#include <sstream>

#include "../include/Maths/Vector3.h"

int GetMiddlePoint(int a_posA, int a_posB, std::vector<Vertex>& a_vertices);

enum class e_indType
{
	VertexInd,
	VertexTexCoordInd,
	VertexNormalInd,
	VertexNormalNoTexCoordInd,
	QuadVertexInd,
	QuadVertexTexCoordInd,
	QuadVertexNormalInd,
	QuadVertexNormalNoTexCoordInd
};

void Model::OBJLoader(const char* a_pFilename)
{
	using namespace DirectX;

	std::fstream file;

	file.open(a_pFilename, std::fstream::in);

	if (file.is_open())
	{
#pragma region Loading
		std::string line;

		std::vector<XMFLOAT3> vertsPos;
		std::vector<XMFLOAT2> vertsTexCoord;
		std::vector<XMFLOAT3> vertsNorm;

		std::vector<WORD> indicies;
		std::vector<WORD> texCoordInd;
		std::vector<WORD> normIndicie;

		while (std::getline(file, line))
		{
			int index = line.find(' ');

			if (index == -1)
			{
				continue;
			}

			std::string subString = line.substr(0, index);
			line.erase(0, index + 1);

			if (subString == "v")
			{
				index = line.find(' ');
				subString = line.substr(0, index);
				line.erase(0, index + 1);

				XMFLOAT3 pos;

				pos.x = (float)atof(subString.c_str());

				index = line.find(' ');
				subString = line.substr(0, index);
				line.erase(0, index + 1);

				pos.y = (float)atof(subString.c_str());

				index = line.find(' ');
				subString = line.substr(0, index);
				line.erase(0, index + 1);

				pos.z = (float)atof(subString.c_str());

				vertsPos.push_back(pos);
			}
			else if (subString == "vt")
			{
				index = line.find(' ');
				subString = line.substr(0, index);
				line.erase(0, index + 1);

				XMFLOAT2 texCoord;

				texCoord.x = (float)atof(subString.c_str());

				index = line.find(' ');
				subString = line.substr(0, index);
				line.erase(0, index + 1);

				texCoord.y = (float)atof(subString.c_str());

				vertsTexCoord.push_back(texCoord);
			}
			else if (subString == "vn")
			{
				index = line.find(' ');
				subString = line.substr(0, index);
				line.erase(0, index + 1);

				XMFLOAT3 pos;

				pos.x = stof(subString);

				index = line.find(' ');
				subString = line.substr(0, index);
				line.erase(0, index + 1);

				pos.y = stof(subString);

				index = line.find(' ');
				subString = line.substr(0, index);
				line.erase(0, index + 1);

				pos.z = stof(subString);

				vertsNorm.push_back(pos);
			}
			else if (subString == "f")
			{
				e_indType type;
				std::vector<std::string> strings;
				
				std::vector<int> untypedInd;
				std::vector<int> untypedTexCoordInd;
				std::vector<int> untypedNormInd;

#pragma region TypeFinding
				int find = -1;
				int oldFind = 0;

				while (true)
				{
					find = line.find(' ', find + 1);

					if (find == -1)
					{
						if (oldFind >= (int)line.size())
						{
							break;
						}
						find = line.size();
					}

					strings.push_back(line.substr(oldFind, find - oldFind));
					oldFind = find + 1;
				}

				for (auto vecIter = strings.begin(); vecIter != strings.end(); vecIter++)
				{
					for (auto strIter = vecIter->cbegin(); strIter != vecIter->cend(); strIter++)
					{
						if ((*strIter) == ' ')
						{
							vecIter->erase(strIter);
						}
					}

					if (vecIter->size() == 0)
					{
						strings.erase(vecIter);
					}
				}

				std::vector<int> indexes;

				while (true)
				{
					find = strings[0].find('/', find + 1);

					if (find == -1)
					{
						break;
					}

					indexes.push_back(find);
				}

				switch (indexes.size())
				{
				case 0:
				{
					if (strings.size() == 3)
					{
						type = e_indType::VertexInd;
					}
					else
					{
						type = e_indType::QuadVertexInd;
					}
					break;
				}
				case 1:
				{
					if (strings.size() == 3)
					{
						type = e_indType::VertexTexCoordInd;
					}
					else
					{
						type = e_indType::QuadVertexTexCoordInd;
					}
					break;
				}
				default:
				{
					if (strings.size() == 3)
					{
						if (indexes[0] + 1 == indexes[1])
						{
							type = e_indType::VertexNormalNoTexCoordInd;
						}
						else
						{
							type = e_indType::VertexNormalInd;
						}
					}
					else
					{
						if (indexes[0] + 1 == indexes[1])
						{
							type = e_indType::QuadVertexNormalNoTexCoordInd;
						}
						else
						{
							type = e_indType::QuadVertexNormalInd;
						}
					}
					break;
				}
				}
#pragma endregion	 

				switch (type)
				{
				case e_indType::VertexInd:
				case e_indType::QuadVertexInd:
				{
					for (auto iter = strings.begin(); iter != strings.end(); iter++)
					{
						untypedInd.push_back(atoi(iter->c_str()) - 1);
					}

					if (type == e_indType::QuadVertexInd)
					{
						indicies.reserve(indicies.size() + 6);

						indicies.push_back(untypedInd[0]);
						indicies.push_back(untypedInd[1]);
						indicies.push_back(untypedInd[2]);

						indicies.push_back(untypedInd[0]);
						indicies.push_back(untypedInd[2]);
						indicies.push_back(untypedInd[3]);

						texCoordInd.reserve(texCoordInd.size() + 6);

						texCoordInd.push_back(-1);
						texCoordInd.push_back(-1);
						texCoordInd.push_back(-1);

						texCoordInd.push_back(-1);
						texCoordInd.push_back(-1);
						texCoordInd.push_back(-1);

						normIndicie.reserve(normIndicie.size() + 6);

						normIndicie.push_back(-1);
						normIndicie.push_back(-1);
						normIndicie.push_back(-1);

						normIndicie.push_back(-1);
						normIndicie.push_back(-1);
						normIndicie.push_back(-1);

						break;
					}

					indicies.reserve(indicies.size() + 3);

					indicies.push_back(untypedInd[0]);
					indicies.push_back(untypedInd[1]);
					indicies.push_back(untypedInd[2]);

					texCoordInd.reserve(texCoordInd.size() + 3);

					texCoordInd.push_back(-1);
					texCoordInd.push_back(-1);
					texCoordInd.push_back(-1);

					normIndicie.reserve(normIndicie.size() + 3);

					normIndicie.push_back(-1);
					normIndicie.push_back(-1);
					normIndicie.push_back(-1);

					break;
				}
				case e_indType::VertexTexCoordInd:
				case e_indType::QuadVertexTexCoordInd:
				{
					for (auto iter = strings.begin(); iter != strings.end(); iter++)
					{
						index = iter->find('/');

						subString = iter->substr(0, index);
						iter->erase(0, index + 1);

						untypedInd.push_back(atoi(subString.c_str()) - 1);
						untypedTexCoordInd.push_back(atoi(iter->c_str()) - 1);
					}

					if (type == e_indType::QuadVertexTexCoordInd)
					{
						indicies.reserve(indicies.size() + 6);

						indicies.push_back(untypedInd[0]);
						indicies.push_back(untypedInd[1]);
						indicies.push_back(untypedInd[2]);

						indicies.push_back(untypedInd[0]);
						indicies.push_back(untypedInd[2]);
						indicies.push_back(untypedInd[3]);

						texCoordInd.reserve(texCoordInd.size() + 6);

						texCoordInd.push_back(untypedTexCoordInd[0]);
						texCoordInd.push_back(untypedTexCoordInd[1]);
						texCoordInd.push_back(untypedTexCoordInd[3]);

						texCoordInd.push_back(untypedTexCoordInd[0]);
						texCoordInd.push_back(untypedTexCoordInd[3]);
						texCoordInd.push_back(untypedTexCoordInd[2]);

						normIndicie.reserve(normIndicie.size() + 6);

						normIndicie.push_back(-1);
						normIndicie.push_back(-1);
						normIndicie.push_back(-1);

						normIndicie.push_back(-1);
						normIndicie.push_back(-1);
						normIndicie.push_back(-1);

						break;
					}

					indicies.reserve(indicies.size() + 3);

					indicies.push_back(untypedInd[0]);
					indicies.push_back(untypedInd[1]);
					indicies.push_back(untypedInd[2]);

					texCoordInd.reserve(texCoordInd.size() + 3);

					texCoordInd.push_back(untypedTexCoordInd[0]);
					texCoordInd.push_back(untypedTexCoordInd[1]);
					texCoordInd.push_back(untypedTexCoordInd[2]);

					normIndicie.reserve(normIndicie.size() + 3);

					normIndicie.push_back(-1);
					normIndicie.push_back(-1);
					normIndicie.push_back(-1);

					break;
				}
				case e_indType::VertexNormalNoTexCoordInd:
				case e_indType::QuadVertexNormalNoTexCoordInd:
				{
					for (auto iter = strings.begin(); iter != strings.end(); iter++)
					{
						index = iter->find('/');

						subString = iter->substr(0, index);
						iter->erase(0, index + 2);

						untypedInd.push_back(atoi(subString.c_str()) - 1);
						untypedNormInd.push_back(atoi(iter->c_str()) - 1);
					}

					if (type == e_indType::QuadVertexNormalNoTexCoordInd)
					{
						indicies.reserve(indicies.size() + 6);

						indicies.push_back(untypedInd[0]);
						indicies.push_back(untypedInd[1]);
						indicies.push_back(untypedInd[2]);

						indicies.push_back(untypedInd[0]);
						indicies.push_back(untypedInd[2]);
						indicies.push_back(untypedInd[3]);

						texCoordInd.reserve(texCoordInd.size() + 6);

						texCoordInd.push_back(-1);
						texCoordInd.push_back(-1);
						texCoordInd.push_back(-1);

						texCoordInd.push_back(-1);
						texCoordInd.push_back(-1);
						texCoordInd.push_back(-1);

						normIndicie.reserve(normIndicie.size() + 6);

						normIndicie.push_back(untypedNormInd[0]);
						normIndicie.push_back(untypedNormInd[1]);
						normIndicie.push_back(untypedNormInd[2]);

						normIndicie.push_back(untypedNormInd[0]);
						normIndicie.push_back(untypedNormInd[2]);
						normIndicie.push_back(untypedNormInd[3]);

						break;
					}

					indicies.reserve(indicies.size() + 3);

					indicies.push_back((WORD)untypedInd[0]);
					indicies.push_back((WORD)untypedInd[1]);
					indicies.push_back((WORD)untypedInd[2]);

					texCoordInd.reserve(texCoordInd.size() + 3);

					texCoordInd.push_back(-1);
					texCoordInd.push_back(-1);
					texCoordInd.push_back(-1);

					normIndicie.reserve(normIndicie.size() + 3);

					normIndicie.push_back(untypedNormInd[0]);
					normIndicie.push_back(untypedNormInd[1]);
					normIndicie.push_back(untypedNormInd[2]);

					break;
				}
				case e_indType::VertexNormalInd:
				case e_indType::QuadVertexNormalInd:
				{
					for (auto iter = strings.begin(); iter != strings.end(); iter++)
					{
						index = iter->find('/');

						subString = iter->substr(0, index);
						iter->erase(0, index + 1);

						untypedInd.push_back(atoi(subString.c_str()) - 1);

						index = iter->find('/');

						subString = iter->substr(0, index);
						iter->erase(0, index + 1);

						untypedTexCoordInd.push_back(atoi(subString.c_str()) - 1);
						untypedNormInd.push_back(atoi(iter->c_str()) - 1);
					}

					if (type == e_indType::QuadVertexNormalInd)
					{
						indicies.reserve(indicies.size() + 6);

						indicies.push_back(untypedInd[0]);
						indicies.push_back(untypedInd[1]);
						indicies.push_back(untypedInd[2]);

						indicies.push_back(untypedInd[0]);
						indicies.push_back(untypedInd[2]);
						indicies.push_back(untypedInd[3]);

						texCoordInd.reserve(texCoordInd.size() + 6);

						texCoordInd.push_back(untypedTexCoordInd[0]);
						texCoordInd.push_back(untypedTexCoordInd[1]);
						texCoordInd.push_back(untypedTexCoordInd[3]);

						texCoordInd.push_back(untypedTexCoordInd[0]);
						texCoordInd.push_back(untypedTexCoordInd[3]);
						texCoordInd.push_back(untypedTexCoordInd[2]);

						normIndicie.reserve(normIndicie.size() + 6);

						normIndicie.push_back(untypedNormInd[0]);
						normIndicie.push_back(untypedNormInd[1]);
						normIndicie.push_back(untypedNormInd[3]);

						normIndicie.push_back(untypedNormInd[0]);
						normIndicie.push_back(untypedNormInd[3]);
						normIndicie.push_back(untypedNormInd[2]);

						break;
					}

					indicies.reserve(indicies.size() + 3);

					indicies.push_back(untypedInd[0]);
					indicies.push_back(untypedInd[1]);
					indicies.push_back(untypedInd[2]);

					texCoordInd.reserve(texCoordInd.size() + 3);

					texCoordInd.push_back(untypedTexCoordInd[0]);
					texCoordInd.push_back(untypedTexCoordInd[1]);
					texCoordInd.push_back(untypedTexCoordInd[2]);

					normIndicie.reserve(normIndicie.size() + 3);

					normIndicie.push_back(untypedNormInd[0]);
					normIndicie.push_back(untypedNormInd[1]);
					normIndicie.push_back(untypedNormInd[2]);

					break;
				}
				}
			}
			else
			{
				continue;
			}
		}

		file.close();

		std::vector<Vertex> vertices;
		std::vector<WORD> mergedIndices;

		/*for (int i = 0; i < (int)vertsPos.size(); i++)
		{
			vertices.push_back({ vertsPos[i], { 0.0f, 1.0f, 0.0f } });
		}*/

		for (unsigned int i = 0; i < indicies.size(); i++)
		{
			Vertex vert;

			if (normIndicie[i] != -1)
			{
				vert = { vertsPos[indicies[i]], vertsNorm[normIndicie[i]] };
			}
			else
			{
				vert = { vertsPos[indicies[i]], {0.0f, 1.0f, 0.0f} };
			}

			bool found = false;

			for (unsigned int j = 0; j < vertices.size(); j++)
			{
				if (vert == vertices[j])
				{
					mergedIndices.push_back(j);
					found = true;
					break;
				}
			}

			if (found)
			{
				continue;
			}

			vertices.push_back(vert);
			mergedIndices.push_back((WORD)vertices.size() - 1);
		}
#pragma endregion

		m_indexCount = indicies.size();

#pragma region Storing
#pragma region Vertices
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(Vertex) * vertices.size();
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		D3D11_MAPPED_SUBRESOURCE ms;
		ZeroMemory(&ms, sizeof(ms));

		HRESULT error;

		error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &m_pVertexBuffer);

		if (FAILED(error))
		{
			MessageBox(NULL, "Failed to create vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
			return;
		}

		error = Application::GetActivePipeline()->m_pDeviceContext->Map(m_pVertexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

		if (FAILED(error))
		{
			MessageBox(NULL, "Failed to open vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
			return;
		}
		memcpy(ms.pData, &vertices[0], vertices.size() * sizeof(Vertex));
		Application::GetActivePipeline()->m_pDeviceContext->Unmap(m_pVertexBuffer, NULL);
#pragma endregion

#pragma region Indicies
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(WORD) * mergedIndices.size();
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		ZeroMemory(&ms, sizeof(ms));

		error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &m_pIndiceBuffer);

		if (FAILED(error))
		{
			MessageBox(NULL, "Failed to create indice buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
			return;
		}

		error = Application::GetActivePipeline()->m_pDeviceContext->Map(m_pIndiceBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

		if (FAILED(error))
		{
			MessageBox(NULL, "Failed to open indice buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
			return;
		}
		memcpy(ms.pData, &mergedIndices[0], mergedIndices.size() * sizeof(WORD));
		Application::GetActivePipeline()->m_pDeviceContext->Unmap(m_pIndiceBuffer, NULL);
#pragma endregion
#pragma endregion
	}
}

Model::Model()
{
	m_pVertexBuffer = nullptr;
	m_pIndiceBuffer = nullptr;

	m_pTexture = nullptr;

	m_pVertexShader = nullptr;
	m_pPixelShader = nullptr;
}
Model::~Model()
{
	if (m_pVertexBuffer)
	{
		m_pVertexBuffer->Release();
	}
	if (m_pIndiceBuffer)
	{
		m_pIndiceBuffer->Release();
	}
}

void Model::SetPixelShader(ID3D11PixelShader* const a_pPixelShader)
{
	m_pPixelShader = a_pPixelShader;
}
void Model::SetVertexShader(ID3D11VertexShader* const a_pVertexShader)
{
	m_pVertexShader = a_pVertexShader;
}

void Model::LoadModel(const char* a_pFileName)
{
	LoadModel(std::string(a_pFileName));
}
void Model::LoadModel(const std::string& a_fileName)
{
	int index = 0;

	while (true)
	{
		int search = a_fileName.find('.', index + 1);

		if (search == -1)
		{
			break;
		}

		index = search;
	}

	std::string subString = a_fileName.substr(index, a_fileName.size() - 1);

	if (subString == ".obj")
	{
		OBJLoader(a_fileName.c_str());
	}
}

int Model::GetIndexCount() const
{
	return m_indexCount;
}

void Model::CreateCube(Model*& a_pModel)
{
	using namespace DirectX;

	if (a_pModel)
	{
		delete a_pModel;
	}

	a_pModel = new Model;

	a_pModel->m_indexCount = 36;

#pragma region Vertices
	Vertex vertices[] =
	{
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f),  XMFLOAT3(0.0f, 1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f),   XMFLOAT3(0.0f, 1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f),    XMFLOAT3(0.0f, 1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f),   XMFLOAT3(0.0f, 1.0f, 0.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f),  XMFLOAT3(0.0f, -1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f),   XMFLOAT3(0.0f, -1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, 1.0f),  XMFLOAT3(0.0f, -1.0f, 0.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f),  XMFLOAT3(-1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f),  XMFLOAT3(-1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f),   XMFLOAT3(-1.0f, 0.0f, 0.0f) },

		{ XMFLOAT3(1.0f, -1.0f, 1.0f),   XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f),  XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f),   XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f),    XMFLOAT3(1.0f, 0.0f, 0.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f),  XMFLOAT3(0.0f, 0.0f, -1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f),   XMFLOAT3(0.0f, 0.0f, -1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f),  XMFLOAT3(0.0f, 0.0f, -1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f),  XMFLOAT3(0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f),   XMFLOAT3(0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f),    XMFLOAT3(0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f),   XMFLOAT3(0.0f, 0.0f, 1.0f) },
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(Vertex) * 24;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	D3D11_MAPPED_SUBRESOURCE ms;
	ZeroMemory(&ms, sizeof(ms));

	HRESULT error;

	error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &a_pModel->m_pVertexBuffer);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to create vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}

	error = Application::GetActivePipeline()->m_pDeviceContext->Map(a_pModel->m_pVertexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to open vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
	memcpy(ms.pData, vertices, sizeof(vertices));
	Application::GetActivePipeline()->m_pDeviceContext->Unmap(a_pModel->m_pVertexBuffer, NULL);
#pragma endregion

#pragma region indicies
	WORD indicies[] =
	{
		3,1,0,
		2,1,3,

		6,4,5,
		7,4,6,

		11,9,8,
		10,9,11,

		14,12,13,
		15,12,14,

		19,17,16,
		18,17,19,

		22,20,21,
		23,20,22
	};

	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(WORD) * 36;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	ZeroMemory(&ms, sizeof(ms));

	error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &a_pModel->m_pIndiceBuffer);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to create index buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}

	error = Application::GetActivePipeline()->m_pDeviceContext->Map(a_pModel->m_pIndiceBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to open index buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
	memcpy(ms.pData, indicies, sizeof(indicies));
	Application::GetActivePipeline()->m_pDeviceContext->Unmap(a_pModel->m_pIndiceBuffer, NULL);
#pragma endregion
}
void Model::CreateQuadPlane(Model*& a_pModel)
{
	using namespace DirectX;

	if (a_pModel)
	{
		delete a_pModel;
	}

	a_pModel = new Model;

	a_pModel->m_indexCount = 6;

#pragma region Vertices
	Vertex vertices[] =
	{
		{ XMFLOAT3(-1.0f, 0.0f, -1.0f),  XMFLOAT3(0.0f, 1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 0.0f, -1.0f),   XMFLOAT3(0.0f, 1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 0.0f, 1.0f),    XMFLOAT3(0.0f, 1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 0.0f, 1.0f),   XMFLOAT3(0.0f, 1.0f, 0.0f) }
	};

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(Vertex) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	D3D11_MAPPED_SUBRESOURCE ms;
	ZeroMemory(&ms, sizeof(ms));

	HRESULT error;

	error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &a_pModel->m_pVertexBuffer);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to create vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}

	error = Application::GetActivePipeline()->m_pDeviceContext->Map(a_pModel->m_pVertexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to open vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
	memcpy(ms.pData, vertices, sizeof(vertices));
	Application::GetActivePipeline()->m_pDeviceContext->Unmap(a_pModel->m_pVertexBuffer, NULL);
#pragma endregion

#pragma region indicies
	WORD indicies[] =
	{
		3,1,0,
		2,1,3,
	};

	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(WORD) * 6;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	ZeroMemory(&ms, sizeof(ms));

	error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &a_pModel->m_pIndiceBuffer);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to create index buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}

	error = Application::GetActivePipeline()->m_pDeviceContext->Map(a_pModel->m_pIndiceBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to open index buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
	memcpy(ms.pData, indicies, sizeof(indicies));
	Application::GetActivePipeline()->m_pDeviceContext->Unmap(a_pModel->m_pIndiceBuffer, NULL);
#pragma endregion
}
void Model::CreateIcoSphere(Model*& a_pModel, int a_recursionLevel)
{
	using namespace DirectX;

	if (a_pModel)
	{
		delete a_pModel;
	}

	a_pModel = new Model;

#pragma region PreVertices
	std::vector<Vertex> vertices;
	vertices.reserve(12);

	float t = (1.0f + sqrt(5.0f)) / 2.0f;

	vertices.push_back({ XMFLOAT3{ -1, t, 0 }, XMFLOAT3{ 1, 0, 1 } });
	vertices.push_back({ XMFLOAT3{ 1, t, 0 }, XMFLOAT3{ 1, 0, 1 } });
	vertices.push_back({ XMFLOAT3{ -1, -t, 0 }, XMFLOAT3{ 1, 0, 1 } });
	vertices.push_back({ XMFLOAT3{ 1, -t, 0 }, XMFLOAT3{ 1, 0, 1 } });

	vertices.push_back({ XMFLOAT3{ 0, -1, t }, XMFLOAT3{ 1, 0, 1 } });
	vertices.push_back({ XMFLOAT3{ 0, 1, t }, XMFLOAT3{ 1, 0, 1 } });
	vertices.push_back({ XMFLOAT3{ 0, -1, -t }, XMFLOAT3{ 1, 0, 1 } });
	vertices.push_back({ XMFLOAT3{ 0, 1, -t }, XMFLOAT3{ 1, 0, 1 } });

	vertices.push_back({ XMFLOAT3{ t, 0, -1 }, XMFLOAT3{ 1, 0, 1 } });
	vertices.push_back({ XMFLOAT3{ t, 0, 1 }, XMFLOAT3{ 1, 0, 1 } });
	vertices.push_back({ XMFLOAT3{ -t, 0, -1 }, XMFLOAT3{ 1, 0, 1 } });
	vertices.push_back({ XMFLOAT3{ -t, 0, 1 }, XMFLOAT3{ 1, 0, 1 } });

	for (auto iter = vertices.begin(); iter != vertices.end(); iter++)
	{
		float mag = sqrt((*iter).pos.x * (*iter).pos.x + (*iter).pos.y * (*iter).pos.y + (*iter).pos.z * (*iter).pos.z);
		if (mag > 0.0f)
		{
			(*iter).pos = { (*iter).pos.x / mag, (*iter).pos.y / mag, (*iter).pos.z / mag };
			iter->normal = iter->pos;
		}
	}
#pragma endregion

#pragma region PreIndicies
	std::vector<WORD> indicies;

	indicies.reserve(60);

	indicies.push_back(0);  indicies.push_back(11); indicies.push_back(5);
	indicies.push_back(0);  indicies.push_back(5);  indicies.push_back(1);
	indicies.push_back(0);  indicies.push_back(1);  indicies.push_back(7);
	indicies.push_back(0);  indicies.push_back(7);  indicies.push_back(10);
	indicies.push_back(0);  indicies.push_back(10); indicies.push_back(11);

	indicies.push_back(1);  indicies.push_back(5);  indicies.push_back(9);
	indicies.push_back(5);  indicies.push_back(11); indicies.push_back(4);
	indicies.push_back(11); indicies.push_back(10); indicies.push_back(2);
	indicies.push_back(10); indicies.push_back(7);  indicies.push_back(6);
	indicies.push_back(7);  indicies.push_back(1);  indicies.push_back(8);

	indicies.push_back(3);  indicies.push_back(9);  indicies.push_back(4);
	indicies.push_back(3);  indicies.push_back(4);  indicies.push_back(2);
	indicies.push_back(3);  indicies.push_back(2);  indicies.push_back(6);
	indicies.push_back(3);  indicies.push_back(6);  indicies.push_back(8);
	indicies.push_back(3);  indicies.push_back(8);  indicies.push_back(9);

	indicies.push_back(4);  indicies.push_back(9);  indicies.push_back(5);
	indicies.push_back(2);  indicies.push_back(4);  indicies.push_back(11);
	indicies.push_back(6);  indicies.push_back(2);  indicies.push_back(10);
	indicies.push_back(8);  indicies.push_back(6);  indicies.push_back(7);
	indicies.push_back(9);  indicies.push_back(8);  indicies.push_back(1);
#pragma endregion

	for (int i = 0; i < a_recursionLevel; i++)
	{
		std::vector<WORD> tempFaces;
		tempFaces.reserve(indicies.size() * 4);

		for (int j = 0; j < (int)indicies.size(); j += 3)
		{
			int indexA = indicies[j];
			int indexB = indicies[j + 1];
			int indexC = indicies[j + 2];

			int a = GetMiddlePoint(indexA, indexB, vertices);
			int b = GetMiddlePoint(indexB, indexC, vertices);
			int c = GetMiddlePoint(indexC, indexA, vertices);

			tempFaces.push_back(indexA); tempFaces.push_back(a); tempFaces.push_back(c);
			tempFaces.push_back(indexB); tempFaces.push_back(b); tempFaces.push_back(a);
			tempFaces.push_back(indexC); tempFaces.push_back(c); tempFaces.push_back(b);
			tempFaces.push_back(a);		 tempFaces.push_back(b); tempFaces.push_back(c);
		}

		indicies = tempFaces;
	}

	a_pModel->m_indexCount = indicies.size();

#pragma region Vertices
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(Vertex) * vertices.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	D3D11_MAPPED_SUBRESOURCE ms;
	ZeroMemory(&ms, sizeof(ms));

	HRESULT error;

	error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &a_pModel->m_pVertexBuffer);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to create vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}

	error = Application::GetActivePipeline()->m_pDeviceContext->Map(a_pModel->m_pVertexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to open vertex buffer" + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
	memcpy(ms.pData, &vertices[0], vertices.size() * sizeof(Vertex));
	Application::GetActivePipeline()->m_pDeviceContext->Unmap(a_pModel->m_pVertexBuffer, NULL);
#pragma endregion

#pragma region Indicies
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(WORD) * indicies.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	ZeroMemory(&ms, sizeof(ms));

	error = Application::GetActivePipeline()->m_pDevice->CreateBuffer(&bd, NULL, &a_pModel->m_pIndiceBuffer);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to create index buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}

	error = Application::GetActivePipeline()->m_pDeviceContext->Map(a_pModel->m_pIndiceBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);

	if (FAILED(error))
	{
		MessageBox(NULL, "Failed to open index buffer: " + error, "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
	memcpy(ms.pData, &indicies[0], indicies.size() * sizeof(WORD));
	Application::GetActivePipeline()->m_pDeviceContext->Unmap(a_pModel->m_pIndiceBuffer, NULL);
#pragma endregion
}

int GetMiddlePoint(int a_posA, int a_posB, std::vector<Vertex>& a_vertices)
{
	using namespace DirectX;

	XMFLOAT3 pos1 = a_vertices[a_posA].pos;
	XMFLOAT3 pos2 = a_vertices[a_posB].pos;
	XMFLOAT3 mid = { (pos1.x + pos2.x) / 2.0f, (pos1.y + pos2.y) / 2.0f, (pos1.z + pos2.z) / 2.0f };

	float mag = sqrt(mid.x * mid.x + mid.y * mid.y + mid.z * mid.z);

	if (mag > 0.0f)
	{
		mid = { mid.x / mag, mid.y / mag, mid.z / mag };
	}

	for (int i = 0; i < (int)a_vertices.size(); i++)
	{
		if (a_vertices[i].pos.x == mid.x && a_vertices[i].pos.y == mid.y && a_vertices[i].pos.z == mid.z)
		{
			return i;
		}
	}

	a_vertices.push_back({ mid, mid });

	return a_vertices.size() - 1;
}
