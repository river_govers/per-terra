#include "../../include/Maths/Maths.h"

float InvSqrt(float a_num)
{
	float xHalf = 0.5f * a_num;
	int i = *(int*)&a_num;
	i = 0x5f375a86 - (i >> 1);
	a_num = *(float*)&i;
	return a_num * (1.5f - xHalf * a_num * a_num);
}

double LerpD(double a_min, double a_max, double a_t)
{
	return a_min + a_t * (a_max - a_min);
}

Vector3 QuaternionToEuler(const DirectX::XMVECTOR& a_quaternion)
{
	using namespace DirectX;

	XMFLOAT4 quaternion = { XMVectorGetX(a_quaternion), XMVectorGetY(a_quaternion), XMVectorGetZ(a_quaternion), XMVectorGetW(a_quaternion) };

	float test = quaternion.x * quaternion.y + quaternion.z * quaternion.w;

	if (test > 0.499f)
	{
		return { XM_PIDIV2, 2.0f * atan2f(quaternion.x, quaternion.w), 0.0f };
	}
	else if (test < -0.499f)
	{
		return { XM_PIDIV2, -2.0f * atan2f(quaternion.x, quaternion.w), 0.0f };
	}

	float sqX = quaternion.x * quaternion.x;
	float sqY = quaternion.y * quaternion.y;
	float sqZ = quaternion.z * quaternion.z;

	return { asinf(2.0f * test), 
			 atan2f(2.0f * quaternion.y * quaternion.w - 2.0f * quaternion.x * quaternion.z, 1.0f - 2.0f * sqY - 2.0f * sqZ), 
			 atan2f(2.0f * quaternion.x * quaternion.w - 2.0f * quaternion.y * quaternion.z, 1.0f - 2.0f * sqX - 2.0f * sqZ) };
}

float Lerp(float a_min, float a_max, float a_t)
{
	return a_min + a_t * (a_max - a_min);
}

//DirectX::XMVECTOR EulerToQuaternion(const DirectX::XMFLOAT3& a_euler)
//{
//	using namespace DirectX;
//
//	if (a_euler.x == 0.0f && a_euler.y == 0.0f && a_euler.z == 0.0f)
//	{
//		return XMQuaternionIdentity();
//	}
//
//	float xCos = cosf(a_euler.x); float xSin = sinf(a_euler.x);
//	float yCos = cosf(a_euler.y); float ySin = sinf(a_euler.y);
//	float zCos = cosf(a_euler.z); float zSin = sinf(a_euler.z);
//
//	return XMQuaternionRotationAxis(XMVectorSet(
//									yCos * zCos * xSin + ySin * zSin * xCos,
//									ySin * zCos * xCos + yCos * zSin * xSin,
//									yCos * zSin * xCos - ySin * zCos * xSin,
//									1.0f),
//									yCos * zCos * xCos - ySin * zSin * xSin);
//}