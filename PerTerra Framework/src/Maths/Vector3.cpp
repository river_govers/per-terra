#include "../../include/Maths/Vector3.h"
			
#include "../../include/Maths/Vector2.h"
			
#include "../../include/Maths/Maths.h"

Vector3::Vector3()
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
}
Vector3::Vector3(const Vector2& a_vector)
{
	x = a_vector.x;
	y = a_vector.y;
	z = 0.0f;
}
Vector3::Vector3(const DirectX::XMFLOAT2 & a_vector)
{
	x = a_vector.x;
	y = a_vector.y;
	z = 0.0f;
}
Vector3::Vector3(const DirectX::XMFLOAT3& a_vector)
{
	x = a_vector.x;
	y = a_vector.y;
	z = a_vector.z;
}
Vector3::Vector3(const DirectX::XMFLOAT4& a_vector)
{
	x = a_vector.x;
	y = a_vector.y;
	z = a_vector.z;
}
Vector3::Vector3(const DirectX::XMVECTOR& a_vector)
{
	using namespace DirectX;

	x = XMVectorGetX(a_vector);
	y = XMVectorGetY(a_vector);
	z = XMVectorGetZ(a_vector);
}
Vector3::Vector3(float a_x, float a_y, float a_z)
{
	x = a_x;
	y = a_y;
	z = a_z;
}

Vector3::operator DirectX::XMFLOAT3() const
{
	return { x, y, z };
}
Vector3::operator DirectX::XMFLOAT4() const
{
	return { x, y, z, 0.0f };
}

Vector3::operator DirectX::XMVECTOR() const
{
	using namespace DirectX;

	return XMVectorSet(x, y, z, 0.0f);
}

Vector3 Vector3::Zero()
{
	return { 0.0f, 0.0f, 0.0f };
}

Vector3 Vector3::Forward()
{
	return { 0.0f, 0.0f, 1.0f };
}

Vector3 Vector3::Backward()
{
	return { 0.0f, 0.0f, -1.0f };
}

Vector3 Vector3::Left()
{
	return { -1.0f, 0.0f, 0.0f };
}

Vector3 Vector3::Right()
{
	return { 1.0f, 0.0f, 0.0f };
}

Vector3 Vector3::Up()
{
	return { 0.0f, 1.0f, 0.0f };
}

Vector3 Vector3::Down()
{
	return { 0.0f, -1.0f, 0.0f };
}

float Vector3::Magnitude() const
{
	return sqrtf(x * x + y * y + z * z);
}

float Vector3::Magnitude(const Vector3 & a_vector)
{
	return a_vector.Magnitude();
}

float Vector3::MagnitudeSqr() const
{
	return x * x + y * y + z * z;
}
float Vector3::MagnitudeSqr(const Vector3 & a_vector)
{
	return a_vector.MagnitudeSqr();
}
float Vector3::MagnitudeInv() const
{
	return InvSqrt(x * x + y * y + z * z);
}

float Vector3::MagnitudeInv(const Vector3 & a_vector)
{
	return a_vector.MagnitudeInv();
}

void Vector3::Normalize()
{
	*this = Normalized();
}

void Vector3::NormalizeInv()
{
	*this = NormalizedInv();
}

Vector3 Vector3::Normalized() const
{
	float mag = Magnitude();

	if (mag == 0.0f)
	{
		return Vector3::Zero();
	}

	return { x / mag, y / mag, z / mag };
}

Vector3 Vector3::Normalized(const Vector3 & a_vector)
{
	return a_vector.Normalized();
}

Vector3 Vector3::NormalizedInv() const
{
	float mag = MagnitudeInv();

	return { x * mag, y * mag, z * mag };
}

Vector3 Vector3::NormalizedInv(const Vector3 & a_vector)
{
	return a_vector.NormalizedInv();
}

Vector3 Vector3::operator *(float a_scalar) const
{
	return { x * a_scalar, y * a_scalar, z * a_scalar };
}

Vector3& Vector3::operator *=(float a_scalar)
{
	x *= a_scalar;
	y *= a_scalar;
	z *= a_scalar;

	return *this;
}

Vector3 Vector3::operator /(float a_scalar) const
{
	return { x / a_scalar, y / a_scalar, z / a_scalar };
}

Vector3& Vector3::operator/=(float a_scalar)
{
	x /= a_scalar;
	y /= a_scalar;
	z /= a_scalar;

	return *this;
}

Vector3 Vector3::operator+(const Vector3 & a_vector) const
{
	return { x + a_vector.x, y + a_vector.y, z + a_vector.z };
}

Vector3& Vector3::operator+=(const Vector3& a_vector)
{
	x += a_vector.x;
	y += a_vector.y;
	z += a_vector.z;

	return *this;
}

Vector3 Vector3::operator-(const Vector3 & a_vector) const
{
	return { x - a_vector.x, y - a_vector.y, z - a_vector.z };
}

Vector3& Vector3::operator-=(const Vector3 & a_vector)
{
	x -= a_vector.x;
	y -= a_vector.y;
	z -= a_vector.z;

	return *this;
}

Vector3 Vector3::operator-()
{
	return { -x, -y, -z };
}

Vector3 Vector3::Cross(const Vector3 & a_vector) const
{
	return { y * a_vector.z - z * a_vector.y, z * a_vector.x - x * a_vector.z, x * a_vector.y - y * a_vector.x };
}

Vector3 Vector3::Cross(const Vector3 & a_vectorA, const Vector3 & a_vectorB)
{
	return a_vectorA.Cross(a_vectorB);
}

float Vector3::Dot(const Vector3& a_vector) const
{
	return x * a_vector.x + y * a_vector.y + z * a_vector.z;
}

float Vector3::Dot(const Vector3 & a_vectorA, const Vector3 & a_vectorB)
{
	return a_vectorA.Dot(a_vectorB);
}

Vector3 operator*(float a_scalar, const Vector3 & a_vector)
{
	return { a_scalar * a_vector.x, a_scalar * a_vector.y, a_scalar * a_vector.z };
}

Vector3 operator/(float a_scalar, const Vector3 & a_vector)
{
	return { a_scalar / a_vector.x, a_scalar / a_vector.y, a_scalar / a_vector.z };
}
