#include "../../include/Maths/Matrix4.h"
#include "../../include/Maths/Vector3.h"


Matrix4::Matrix4()
{
	*this = Identity();
}
Matrix4::Matrix4(const DirectX::XMMATRIX& a_matrix)
{
	using namespace DirectX;
	
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			ind2D[i][j] = XMVectorGetByIndex(a_matrix.r[j], i);
		}
	}
}
Matrix4::Matrix4(const DirectX::XMFLOAT4X4& a_matrix)
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			ind2D[i][j] = a_matrix.m[i][j];
		}
	}
}
Matrix4::Matrix4(float a_00, float a_01, float a_02, float a_03, float a_10, float a_11, float a_12, float a_13, float a_20, float a_21, float a_22, float a_23, float a_30, float a_31, float a_32, float a_33)
{
	ind_00 = a_00;
	ind_01 = a_01;
	ind_02 = a_02;
	ind_03 = a_03;

	ind_10 = a_10;
	ind_11 = a_11;
	ind_12 = a_12;
	ind_13 = a_13;

	ind_20 = a_20;
	ind_21 = a_21;
	ind_22 = a_22;
	ind_23 = a_23;

	ind_30 = a_30;
	ind_31 = a_31;
	ind_32 = a_32;
	ind_33 = a_33;

}
Matrix4::Matrix4(const Matrix4& a_matrix)
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			ind2D[i][j] = a_matrix.ind2D[i][j];
		}
	}

}

//Matrix4::operator DirectX::XMMATRIX() const
//{
//	using namespace DirectX;
//
//	return { ind_00, ind_10, ind_20, ind_30,
//			 ind_01, ind_11, ind_21, ind_31,
//			 ind_02, ind_12, ind_22, ind_32,
//			 ind_03, ind_13, ind_23, ind_33 };
//}
Matrix4::operator DirectX::XMFLOAT4X4() const
{
	using namespace DirectX;

	XMFLOAT4X4 matrix;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			matrix.m[i][j] = ind2D[i][j];
		}
	}

	return matrix;
}

DirectX::XMMATRIX Matrix4::ToXMMatrix(bool a_transpose) const
{
	if (a_transpose)
	{
		return { ind_00, ind_10, ind_20, ind_30,
				 ind_01, ind_11, ind_21, ind_31,
				 ind_02, ind_12, ind_22, ind_32,
				 ind_03, ind_13, ind_23, ind_33 };
	}

	return { ind_00, ind_01, ind_02, ind_03,
			 ind_10, ind_11, ind_12, ind_13,
			 ind_20, ind_21, ind_22, ind_23,
			 ind_30, ind_31, ind_32, ind_33 };
}
Matrix4 Matrix4::operator *(const Matrix4& a_matrix) const
{
	Matrix4 temp = Matrix4();

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			float value = 0.0f;

			for (int k = 0; k < 4; k++)
			{
				value += ind2D[i][k] * a_matrix.ind2D[k][j];
			}

			temp.ind2D[i][j] = value;
		}
	}

	return temp;
}
Matrix4& Matrix4::operator *=(const Matrix4& a_matrix)
{
	Matrix4 temp = Matrix4();

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			float value = 0.0f;

			for (int k = 0; k < 4; k++)
			{
				value += ind2D[i][k] * a_matrix.ind2D[k][j];
			}

			temp.ind2D[i][j] = value;
		}
	}

	return *this = temp;
}

Vector3 Matrix4::operator*(const Vector3 & a_vector) const
{
	return Vector3(a_vector.x * ind_00 + a_vector.y * ind_01 + a_vector.z * ind_02 + ind_03,
				   a_vector.x * ind_10 + a_vector.y * ind_11 + a_vector.z * ind_12 + ind_13,
				   a_vector.x * ind_20 + a_vector.y * ind_21 + a_vector.z * ind_22 + ind_23);
}

Matrix4 Matrix4::operator -(const Matrix4& a_matrix) const
{
	return { ind_00 - a_matrix.ind_00, ind_01 - a_matrix.ind_01, ind_02 - a_matrix.ind_02, ind_03 - a_matrix.ind_03,
			 ind_10 - a_matrix.ind_10, ind_11 - a_matrix.ind_11, ind_12 - a_matrix.ind_12, ind_13 - a_matrix.ind_13,
			 ind_20 - a_matrix.ind_20, ind_21 - a_matrix.ind_21, ind_22 - a_matrix.ind_22, ind_23 - a_matrix.ind_23,
			 ind_30 - a_matrix.ind_30, ind_31 - a_matrix.ind_31, ind_32 - a_matrix.ind_32, ind_33 - a_matrix.ind_33 };
}
Matrix4& Matrix4::operator -=(const Matrix4 & a_matrix)
{
	return *this = *this - a_matrix;
}
Matrix4 Matrix4::operator +(const Matrix4 & a_matrix) const
{
	return { ind_00 + a_matrix.ind_00, ind_01 + a_matrix.ind_01, ind_02 + a_matrix.ind_02, ind_03 + a_matrix.ind_03,
			 ind_10 + a_matrix.ind_10, ind_11 + a_matrix.ind_11, ind_12 + a_matrix.ind_12, ind_13 + a_matrix.ind_13,
			 ind_20 + a_matrix.ind_20, ind_21 + a_matrix.ind_21, ind_22 + a_matrix.ind_22, ind_23 + a_matrix.ind_23,
			 ind_30 + a_matrix.ind_30, ind_31 + a_matrix.ind_31, ind_32 + a_matrix.ind_32, ind_33 + a_matrix.ind_33 };
}
Matrix4& Matrix4::operator +=(const Matrix4 & a_matrix)
{
	return *this = *this + a_matrix;
}

Matrix4 Matrix4::Identity()
{
	return { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };
}

Matrix4 Matrix4::FromQuaternion(const DirectX::XMVECTOR & a_quaternion)
{
	using namespace DirectX;

	float x = XMVectorGetX(a_quaternion);
	float y = XMVectorGetY(a_quaternion);
	float z = XMVectorGetZ(a_quaternion);
	float w = XMVectorGetW(a_quaternion);

	// Equation
	// 1 - 2*qy2 - 2*qz2		2*qx*qy - 2*qz*qw		2*qx*qz + 2*qy*qw
	// 2 * qx*qy + 2 * qz*qw	1 - 2 * qx2 - 2 * qz2	2 * qy*qz - 2 * qx*qw
	// 2 * qx*qz - 2 * qy*qw	2 * qy*qz + 2 * qx*qw	1 - 2 * qx2 - 2 * qy2

	return { 1.0f - 2.0f * (y * y) - 2.0f * (z * z), 2.0f * x * y - 2.0f * z * w,			 2.0f * x * z + 2.0f * y * w,			 0.0f,
			 2.0f * x * y + 2.0f * z * w,			 1.0f - 2.0f * (x * x) - 2.0f * (z * z), 2.0f * y * z - 2.0f * x * w,			 0.0f,
			 2.0f * x * z - 2.0f * y * w,			 2.0f * y * z + 2.0f * x * w,			 1.0f - 2.0f * (x * x) - 2.0f * (y * y), 0.0f,
			 0.0f,									 0.0f,									 0.0f,									 1.0f };
}

Matrix4& Matrix4::Transpose()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = i; j < 4; j++)
		{
			float temp = ind2D[i][j];

			ind2D[i][j] = ind2D[j][i];
			ind2D[j][i] = temp;
		}
	}

	return *this;
}
Matrix4 Matrix4::Transposed(const Matrix4& a_matrix)
{
	Matrix4 temp = a_matrix;

	temp.Transpose();

	return temp;
}
Matrix4 Matrix4::Transposed() const
{
	Matrix4 temp = *this;

	temp.Transpose();

	return temp;
}

Matrix4 Matrix4::Inversed() const
{
	float mat[4][8];

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			mat[i][j] = ind2D[i][j];
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 4; j < 8; ++j)
		{
			if (i == (j - 4))
			{
				mat[i][j] = 1.0f;
			}
			else
			{
				mat[i][j] = 0.0f;
			}
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if (i != j)
			{
				float ratio = mat[j][i] / mat[i][i];

				for (int k = 0; k < 8; ++k)
				{
					mat[j][k] -= ratio * mat[i][k];
				}
			}
		}
	}

	for (int i = 0; i < 4; ++i)
	{
		float inv = mat[i][i];

		for (int j = 0; j < 8; ++j)
		{
			mat[i][j] /= inv;
		}
	}

	Matrix4 tempMat;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 4; j < 8; ++j)
		{
			tempMat.ind2D[i][j - 4] = mat[i][j];
		}
	}

	return tempMat;
}
Matrix4 & Matrix4::Inverse()
{
	return *this = Inversed();
}
