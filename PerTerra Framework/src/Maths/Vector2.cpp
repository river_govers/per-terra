#include "../../include/Maths/Vector2.h"
			
#include "../../include/Maths/Vector3.h"
			
#include "../../include/Maths/Maths.h"

Vector2::operator DirectX::XMFLOAT2()
{
	return { x, y };
}

Vector2::operator DirectX::XMFLOAT3()
{
	return { x, y, 0.0f };
}

Vector2::operator DirectX::XMVECTOR()
{
	using namespace DirectX;

	return XMVectorSet(x, y, 0.0f, 0.0f);
}

Vector2::operator Vector3()
{
	return { x, y, 0.0f };
}

Vector2::Vector2()
{
	x = 0.0f;
	y = 0.0f;
}
Vector2::Vector2(const Vector2& a_vector)
{
	x = a_vector.x;
	y = a_vector.y;
}
Vector2::Vector2(const DirectX::XMFLOAT2& a_vector)
{
	x = a_vector.x;
	y = a_vector.y;
}
Vector2::Vector2(const DirectX::XMVECTOR& a_vector)
{
	using namespace DirectX;

	x = XMVectorGetX(a_vector);
	y = XMVectorGetY(a_vector);
}
Vector2::Vector2(float a_x, float a_y)
{
	x = a_x;
	y = a_y;
}

float Vector2::Magnitude() const
{
	return sqrtf(x * x + y * y);
}

float Vector2::MagnitudeSqr() const
{
	return x * x + y * y;
}

float Vector2::MagnitudeInv() const
{
	return InvSqrt(x * x + y * y);
}

Vector2 Vector2::Normalized() const
{
	float mag = Magnitude();

	if (mag == 0.0f)
	{
		return Vector2::Zero();
	}
	return { x / mag, y / mag };
}

Vector2 Vector2::NormalizedInv() const
{
	float mag = MagnitudeInv();

	return { x * mag, y * mag };
}

Vector2 Vector2::operator*(float a_scalar) const
{
	return { x * a_scalar, y * a_scalar };
}

Vector2 & Vector2::operator*=(float a_scalar)
{
	x *= a_scalar;
	y *= a_scalar;

	return *this;
}

Vector2 Vector2::operator+(const Vector2 & a_vector) const
{
	return { x + a_vector.x, y + a_vector.y };
}
Vector2 & Vector2::operator+=(const Vector2 & a_vector)
{
	x += a_vector.x;
	y += a_vector.y;

	return *this;
}
Vector2 Vector2::operator-(const Vector2 & a_vector) const
{
	return { x - a_vector.x, y - a_vector.y };
}

Vector2 Vector2::operator-=(const Vector2 & a_vector)
{
	x -= a_vector.x;
	y -= a_vector.y;

	return *this;
}

Vector2 Vector2::operator-() const
{
	return { -x, -y };
}

float Vector2::Dot(const Vector2 & a_vector)
{
	return x * a_vector.x + y * a_vector.y;
}

Vector2 Vector2::Zero()
{
	return {0.0f, 0.0f};
}

Vector2 operator*(float a_sclar, const Vector2 & a_vector)
{
	return { a_sclar * a_vector.x, a_sclar * a_vector.y };
}
