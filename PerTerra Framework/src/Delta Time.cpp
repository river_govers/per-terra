#include "../include/Delta Time.h"

float Time::m_deltaTime = 0.0f;
float Time::m_scale = 1.0f;

float Time::GetScale()
{
	return m_scale;
}
float Time::GetDeltaTime()
{
	return m_deltaTime * m_scale;
}
float Time::GetDeltaTimeUnscaled()
{
	return m_deltaTime;
}

void Time::SetScale(float a_scale)
{
	m_scale = a_scale;
}
