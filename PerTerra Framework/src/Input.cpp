#include "../include/Input.h"

#include "../include/Application.h"
#include "../include/Pipeline.h"

#pragma region Singletons
Input* Input::m_pInput = nullptr;

void Input::CreateInputManager()
{
	m_pInput = new Input();
}
void Input::DestroyInputManager()
{
	delete m_pInput;
}

Input* Input::GetSingleton()
{
	return m_pInput;
}
#pragma endregion

Input::Input()
{
	m_keyState.resize(256);

	timeOut = 0;

	for (auto iter = m_keyState.begin(); iter != m_keyState.end(); ++iter)
	{
		(*iter) = e_KeyState::Up;
	}

	m_buttonState.resize(3);

	for (auto iter = m_buttonState.begin(); iter != m_buttonState.end(); ++iter)
	{
		(*iter) = e_KeyState::Up;
	}
}
Input::~Input()
{
}

#pragma region Update
#pragma region MouseUpdate
void Input::FunctionKeyDown(e_key a_button)
{
	m_keyState[(int)a_button] = e_KeyState::Pressed;
}
void Input::OnMouseDown(e_mouseButton a_button)
{
	m_buttonState[(int)a_button] = e_KeyState::Pressed;
}
void Input::OnMouseUp(e_mouseButton a_button)
{
	m_buttonState[(int)a_button] = e_KeyState::Released;
}
#pragma endregion
#pragma region KeyBoardUpdate
void Input::OnKeyDown(const WPARAM& a_param)
{
	char c = MapVirtualKey(a_param, MAPVK_VK_TO_CHAR);

	if (c != 0)
	{
		m_keyState[c] = e_KeyState::Pressed;
	}
	else
	{
		m_keyState[a_param] = e_KeyState::Pressed;
	}
}
void Input::OnKeyUp(const WPARAM& a_param)
{
	char c = MapVirtualKey(a_param, MAPVK_VK_TO_CHAR);

	if (c != 0)
	{
		m_keyState[c] = e_KeyState::Released;
	}
	else
	{
		m_keyState[a_param] = e_KeyState::Released;
	}
}
#pragma endregion

void Input::Update()
{
	for (int i = 0; i < (int)m_keyState.size(); i++)
	{
		switch (m_keyState[i])
		{
		case e_KeyState::Pressed:
			m_keyState[i] = e_KeyState::Down;
			break;
		case e_KeyState::Released:
			m_keyState[i] = e_KeyState::Up;
			break;
		}
	}

	for (auto iter = m_buttonState.begin(); iter != m_buttonState.end(); ++iter)
	{
		switch (*iter)
		{
		case e_KeyState::Pressed:
			(*iter) = e_KeyState::Down;
			break;
		case e_KeyState::Released:
			(*iter) = e_KeyState::Up;
			break;
		}
	}

	if (m_locked && timeOut > 3)
	{
		Vector2 pos = Application::GetActiveApplication()->GetWindowPosition();
		pos = { pos.x + (Application::GetActivePipeline()->GetViewWidth() / 2.0f), pos.y + (Application::GetActivePipeline()->GetViewHeight() / 2.0f) };
		SetCursorPos((int)pos.x, (int)pos.y);
		m_oldMousePos = pos;
		timeOut = 0;
	}
	else
	{
		m_oldMousePos = m_mousePos;
	}

	timeOut++;

	POINT pos;

	GetCursorPos(&pos);

	m_mousePos.x = (float)pos.x;
	m_mousePos.y = (float)pos.y;
}
#pragma endregion

#pragma region Retrievers
#pragma region MouseRetrievers
bool Input::IsMousePressed(e_mouseButton a_mouseButton) const
{
	if (m_buttonState[(int)a_mouseButton] == e_KeyState::Pressed)
	{
		return true;
	}

	return false;
}
bool Input::IsMouseDown(e_mouseButton a_mouseButton) const
{
	if (m_buttonState[(int)a_mouseButton] == e_KeyState::Pressed || m_buttonState[(int)a_mouseButton] == e_KeyState::Down)
	{
		return true;
	}

	return false;
}
bool Input::IsMouseReleased(e_mouseButton a_mouseButton) const
{
	if (m_buttonState[(int)a_mouseButton] == e_KeyState::Released)
	{
		return true;
	}

	return false;
}
bool Input::IsMouseUp(e_mouseButton a_mouseButton) const
{
	if (m_buttonState[(int)a_mouseButton] == e_KeyState::Released || m_buttonState[(int)a_mouseButton] == e_KeyState::Up)
	{
		return true;
	}

	return false;
}

Vector2 Input::GetMousePos() const
{

	Vector2 winPos = Application::GetActiveApplication()->GetWindowPosition();

	return { m_mousePos.x - winPos.x, m_mousePos.y - winPos.y };
}
Vector2 Input::GetDesktopMousePos() const
{
	return m_mousePos;
}

Vector2 Input::GetMouseMovement() const
{
	return { m_oldMousePos.x - m_mousePos.x, m_oldMousePos.y - m_mousePos.y };
}
#pragma endregion

#pragma region KeyboardRetrievers
bool Input::IsKeyPressed(e_key a_key) const
{
	if (m_keyState[(int)a_key] == e_KeyState::Pressed)
	{
		return true;
	}

	return false;
}
bool Input::IsKeyDown(e_key a_key) const
{
	if (m_keyState[(int)a_key] == e_KeyState::Pressed || m_keyState[(int)a_key] == e_KeyState::Down)
	{
		return true;
	}

	return false;
}
bool Input::IsKeyReleased(e_key a_key) const 
{
	if (m_keyState[(int)a_key] == e_KeyState::Released)
	{
		return true;
	}

	return false;
}
bool Input::IsKeyUp(e_key a_key) const
{
	if (m_keyState[(int)a_key] == e_KeyState::Released || m_keyState[(int)a_key] == e_KeyState::Up)
	{
		return true;
	}

	return false;
}
#pragma endregion
#pragma endregion

void Input::SetCursorLock(bool a_state)
{
	m_locked = a_state;
}
bool Input::GetCursorLockState() const
{
	return m_locked;
}

void Input::SetCursorVisiblity(bool a_state) const
{
	if (a_state)
	{
		while (ShowCursor(TRUE) < 1);
	}
	else
	{
		while (ShowCursor(FALSE) >= 0);
	}
}
