#include "../include/Entity.h"

#include "../include/Application.h"
#include "../include/Pipeline.h"

void Entity::Draw()
{
	Application::GetActivePipeline()->DrawModel(m_pTransform->ToMatrix(), *m_model);
}

Entity::Entity(ChunkManager* a_chunkManager, bool a_static) : m_static(a_static)
{
	m_pTransform = new Transform(this, a_chunkManager);
}

Entity::~Entity()
{
	delete m_pTransform;
}

void Entity::SetModel(Model* const a_model)
{
	m_model = a_model;
}
