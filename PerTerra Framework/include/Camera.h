#pragma once

#include <list>

#include "Transform.h"

#include "D3D11.h"
#include "D3DX11.h"
#include "D3DX10.h"

class Camera
{
private:
	static std::list<Camera*> m_cameraList;
	static Camera* m_pActiveCamera;
	static Camera* m_pDrawingCamera;
protected:

public:
	static Camera& GetActiveCamera();
	static void SetActiveCamera(Camera& a_camera);
	static std::list<Camera*>& GetCameraList();
private:
	friend class Pipeline;

	D3D11_VIEWPORT m_viewport;
	ID3D11RenderTargetView* m_renderTarget = nullptr;

	D3DXCOLOR m_clearColor;

	UINT m_clearFlags;

	Matrix4 m_view;
	Matrix4 m_projection;
	Transform m_transform;

	bool m_clearTexture;
protected:

public:
	Camera(bool a_clearTexture);
	Camera(bool a_clearTexture, float a_fovAngle);
	Camera(bool a_clearTexture, float a_fovAngle, const D3DXCOLOR& a_clearColor, UINT a_clearFlags);
	Camera(bool a_clearTexture, const Vector2& a_viewportStart, const Vector2& a_viewPortSize, float a_fovAngle);
	Camera(bool a_clearTexture, const Vector2& a_viewportStart, const Vector2& a_viewPortSize, float a_fovAngle, const D3DXCOLOR& a_clearColor, UINT a_clearFlags);
	Camera(bool a_clearTexture, const Vector2& a_viewportStart, const Vector2& a_viewPortSize, float a_minDepth, float a_maxDepth, float a_fovAngle);
	Camera(bool a_clearTexture, const Vector2& a_viewportStart, const Vector2& a_viewPortSize, float a_minDepth, float a_maxDepth, float a_fovAngle, const D3DXCOLOR& a_clearColor, UINT a_clearFlags);
	~Camera();

	Transform& Transform();

	Matrix4 DrawMatrix() const;
	Matrix4 ViewMatrix() const;

	Vector3 ScreenToWorld(const Vector3& a_position) const;
};