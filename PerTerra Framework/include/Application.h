#pragma once

class Pipeline;

#include <Windows.h>

#include "Maths\Vector2.h"

class Application
{
private:
	friend class Pipeline;

	static Application* m_pActiveApplication;

	Pipeline* m_pPipeline;

	HWND m_window;
	Vector2 m_windowPos;
protected:
	virtual void Update() { };
	virtual void Draw(Pipeline* a_pPipeline) { };
	virtual void LineDraw(Pipeline* a_pPipeline) { };
public:
	Application(int a_screenWidth, int a_screenHeight, const HINSTANCE& a_instance, int a_nCmdShow, const char* a_pApplicationName);
	virtual ~Application();

	/// <summary>Do not call it is already running you will only destroy stuff</summary>
	int Run();

	Vector2 GetWindowPosition();

	static Application* GetActiveApplication();
	static Pipeline* GetActivePipeline();

	void SetWindowPosition(const Vector2& a_pos);

	HWND& GetWindow();

	// Windows is likely to call this function in the middle of construction be warned
	/// <summary>Call when moving the window to update the window pos</summary>
	void UpdateWindowPosition();

	void Quit();
};

LRESULT CALLBACK WindowProc(HWND a_hwnd, UINT a_message, WPARAM a_wParam, LPARAM a_lParam);