#pragma once

#include "Maths\Vector2.h"

#include <list>

class Terrain;
class Entity;

struct Chunk
{
	Terrain* m_pTerrain;
	Vector2 m_pos;

	std::list<Entity*> m_entities;

	Chunk(float a_terrainSize, int a_terrainResolution, Vector2 a_pos);
	~Chunk();

	void RegisterEntity(Entity* a_entity);
	void RemoveEntity(Entity* a_entity);
};

class ChunkManager
{
private:
	int m_indicies;
	Chunk*** m_pChunks;
	float m_chunkSize;
	int m_chunkRes;
	float m_chunkScale;
protected:

public:
	ChunkManager(int a_chunks, float a_terrainSize, int a_terrainResolution);
	~ChunkManager();

	Chunk& GetChunk(int a_index);
	Chunk& GetChunk(int a_x, int a_y);
	Chunk& GetChunk(const Vector2& a_pos);

	Vector2 GetChunkSize() const;

	void Update();

	void Draw(int a_indicies, int a_x, int a_y);
	void Draw(int a_indicies, const Vector2& a_pos);
	void DrawWireFrame(int a_indicies, int a_x, int a_y);
	void DrawWireFrame(int a_indicies, const Vector2& a_pos);
	void DrawDebug(int a_indicies, int a_x, int a_y);
	void DrawDebug(int a_indicies, const Vector2& a_pos);
};
