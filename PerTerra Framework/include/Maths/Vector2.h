#pragma once

#include <DirectXMath.h>

struct Vector3;

struct Vector2
{
	float x, y;

	Vector2();
	Vector2(const Vector2& a_vector);
	Vector2(const DirectX::XMFLOAT2& a_vector);
	Vector2(const DirectX::XMVECTOR& a_vector);
	Vector2(float a_x, float a_y);

	explicit operator DirectX::XMFLOAT2();
	explicit operator DirectX::XMFLOAT3();

	explicit operator DirectX::XMVECTOR();

	explicit operator Vector3();

	float Magnitude() const;
	float MagnitudeSqr() const;
	float MagnitudeInv() const;

	Vector2 Normalized() const;
	Vector2 NormalizedInv() const;

	Vector2 operator *(float a_scalar) const;
	Vector2& operator *=(float a_scalar);

	Vector2 operator +(const Vector2& a_vector) const;
	Vector2& operator +=(const Vector2& a_vector);
	Vector2 operator -(const Vector2& a_vector) const;
	Vector2 operator -=(const Vector2& a_vector);

	Vector2 operator -() const;

	float Dot(const Vector2& a_vector);

	static Vector2 Zero();
};

Vector2 operator *(float a_sclar, const Vector2& a_vector);