#pragma once

#include <DirectXMath.h>

#include "Vector2.h"
#include "Vector3.h"

#include "Matrix4.h"

float InvSqrt(float a_num);

float Lerp(float a_min, float a_max, float a_t);
double LerpD(double a_min, double a_max, double a_t);

Vector3 QuaternionToEuler(const DirectX::XMVECTOR& a_quaternion);

//DirectX::XMVECTOR EulerToQuaternion(const DirectX::XMFLOAT3& a_euler);