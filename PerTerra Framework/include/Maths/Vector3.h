#pragma once

#include <DirectXMath.h>

struct Vector2;

struct Vector3
{
	float x, y, z;

	Vector3();
	Vector3(const Vector2& a_vector);
	Vector3(const DirectX::XMFLOAT2& a_vector);
	Vector3(const DirectX::XMFLOAT3& a_vector);
	Vector3(const DirectX::XMFLOAT4& a_vector);
	Vector3(const DirectX::XMVECTOR& a_vector);
	Vector3(float a_x, float a_y, float a_z);

	explicit operator DirectX::XMFLOAT3() const;
	explicit operator DirectX::XMFLOAT4() const;

	explicit operator DirectX::XMVECTOR() const;

	static Vector3 Zero();
	static Vector3 Forward();
	static Vector3 Backward();
	static Vector3 Left();
	static Vector3 Right();
	static Vector3 Up();
	static Vector3 Down();

	float Magnitude() const;
	static float Magnitude(const Vector3& a_vector);
	float MagnitudeSqr() const;
	static float MagnitudeSqr(const Vector3& a_vector);
	float MagnitudeInv() const;
	static float MagnitudeInv(const Vector3& a_vector);

	void Normalize();
	void NormalizeInv();

	Vector3 Normalized() const;
	static Vector3 Normalized(const Vector3& a_vector);
	Vector3 NormalizedInv() const;
	static Vector3 NormalizedInv(const Vector3& a_vector);

	Vector3 operator *(float a_scalar) const;
	Vector3& operator *=(float a_scalar);
	Vector3 operator /(float a_scalar) const;
	Vector3& operator /=(float a_scalar);

	Vector3 operator +(const Vector3& a_vector) const;
	Vector3& operator +=(const Vector3& a_vector);
	Vector3 operator -(const Vector3& a_vector) const;
	Vector3& operator -=(const Vector3& a_vector);

	Vector3 operator -();

	Vector3 Cross(const Vector3& a_vector) const;
	static Vector3 Cross(const Vector3& a_vectorA, const Vector3& a_vectorB);
	float Dot(const Vector3& a_vector) const;
	static float Dot(const Vector3& a_vectorA, const Vector3& a_vectorB);
};

Vector3 operator *(float a_scalar, const Vector3& a_vector);
Vector3 operator /(float a_scalar, const Vector3& a_vector);