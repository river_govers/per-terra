#pragma once

#include <DirectXMath.h>

struct Vector3;

struct Matrix4
{
	union 
	{
		float ind2D[4][4];

		struct 
		{
			float ind_00, ind_01, ind_02, ind_03;
			float ind_10, ind_11, ind_12, ind_13;
			float ind_20, ind_21, ind_22, ind_23;
			float ind_30, ind_31, ind_32, ind_33;
		};

		float ind[16];
	};

	Matrix4();
	Matrix4(const DirectX::XMMATRIX& a_matrix);
	Matrix4(const DirectX::XMFLOAT4X4& a_matrix);
	Matrix4(float a_00, float a_01, float a_02, float a_03, float a_10, float a_11, float a_12, float a_13, float a_20, float a_21, float a_22, float a_23, float a_30, float a_31, float a_32, float a_33);
	Matrix4(const Matrix4& a_matrix);

	//explicit operator DirectX::XMMATRIX() const;
	explicit operator DirectX::XMFLOAT4X4() const;

	DirectX::XMMATRIX ToXMMatrix(bool a_transpose = true) const;

	Matrix4 operator *(const Matrix4& a_matrix) const;
	Matrix4& operator *=(const Matrix4& a_matrix);
	Vector3 operator *(const Vector3& a_vector) const;

	Matrix4 operator -(const Matrix4& a_matrix) const;
	Matrix4& operator -=(const Matrix4& a_matrix);
	Matrix4 operator +(const Matrix4& a_matrix) const;
	Matrix4& operator +=(const Matrix4& a_matrix);

	static Matrix4 Identity();
	static Matrix4 FromQuaternion(const DirectX::XMVECTOR& a_quaternion);

	Matrix4& Transpose();
	static Matrix4 Transposed(const Matrix4& a_matrix);
	Matrix4 Transposed() const;

	Matrix4 Inversed() const;
	Matrix4& Inverse();
};