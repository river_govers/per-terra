#pragma once

#include "D3D11.h"
#include "D3DX11.h"
#include "D3DX10.h"

#include <DirectXMath.h>

#ifdef _DEBUG
#include <dxgidebug.h>
#endif // _DEBUG

#include "../include/Maths/Vector2.h"

struct Vector3;
struct Matrix4;

class Model;
struct Vertex;

class Pipeline
{
private:
	friend class Texture;
	friend class Model;
	friend class Terrain;
	friend class Camera;

	// TODO: Swap with proper constant buffer struct
	struct ConstantBuffer
	{
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX m_view;
		DirectX::XMMATRIX m_projection;
		DirectX::XMFLOAT4 lightDirs[2];
		D3DXCOLOR lightColors[2];
		D3DXCOLOR outColor;
	};

	IDXGISwapChain* m_pSwapChain = nullptr;
	ID3D11Device* m_pDevice = nullptr;
	ID3D11DeviceContext* m_pDeviceContext = nullptr;

	ID3D11RenderTargetView* m_pBackBuffer = nullptr;
	ID3D11Texture2D* m_pDepthStencil = nullptr;
	ID3D11DepthStencilView* m_pDepthStencilView = nullptr;
	ID3D11Buffer* m_pConstantBuffer = nullptr;
	ID3D11InputLayout* m_pLayout = nullptr;

	ID3D11RasterizerState* m_pSolidRasterizerState = nullptr;
	ID3D11RasterizerState* m_pWireRasterizerState = nullptr;

	ID3D11Buffer* m_pLineBuffer = nullptr;
	ID3D11Buffer* m_pLineIndBuffer = nullptr;

#ifdef _DEBUG
	ID3D11Debug* m_d3dDebug = nullptr;
#endif // _DEBUG

	// TODO: Create Materials
	// Default shaders
	ID3D11VertexShader* m_pVertexShader = nullptr;
	ID3D11PixelShader* m_pPixelShader = nullptr;
	ID3D11PixelShader* m_pSolidPixelShader = nullptr;

	Vector2 m_screenDimension;
private:
	friend class Application;

	Pipeline(const HWND& a_window, int a_screenWidth, int a_screenHeight);
	~Pipeline();

	bool m_success;

	bool InitD3D(const HWND& a_window);
	bool InitPipeline();
	bool InitGraphics();
	
	void DrawModel(const DirectX::XMMATRIX& a_transform, const Model& a_model);
	void DrawModelWireFrame(const DirectX::XMMATRIX& a_transform, const Model& a_model, const D3DXCOLOR& a_color = { 1.0f, 1.0f, 1.0f, 1.0f });
	void DrawTerrain(const DirectX::XMMATRIX& a_transform, const Terrain& a_terrain);
	void DrawTerrainWireFrame(const DirectX::XMMATRIX& a_transform, const Terrain& a_terrain, const D3DXCOLOR& a_color = { 1.0f, 1.0f, 1.0f, 1.0f });

	void DrawLine(const DirectX::XMFLOAT3& a_startPos, const DirectX::XMFLOAT3& a_endPos, const D3DXCOLOR& a_color);
protected:
	void DrawingCycle();

	void OpenPipeline();
	void SwitchDrawingTopology();
	void ClosePipeline();
public:
	int GetViewWidth() const;
	int GetViewHeight() const;
	Vector2 GetViewDimensions() const;

	void DrawModel(const Matrix4& a_transform, const Model& a_model);
	void DrawModelWireFrame(const Matrix4& a_transform, const Model& a_model, const D3DXCOLOR& a_color = { 1.0f, 1.0f, 1.0f, 1.0f});
	void DrawTerrain(const Matrix4& a_transform, const Terrain& a_terrain);
	void DrawTerrainWireFrame(const Matrix4& a_transform, const Terrain& a_terrain, const D3DXCOLOR& a_color = { 1.0f, 1.0f, 1.0f, 1.0f });

	void DrawLine(const Vector3& a_startPos, const Vector3& a_endPos, const D3DXCOLOR& a_color = { 1.0f, 1.0f, 1.0f, 1.0f });
};