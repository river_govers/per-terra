#pragma once

#include <Windows.h>

#include <vector>

//#include <DirectXMath.h>

#include "Maths/Vector2.h"

enum class e_mouseButton
{
	Left,
	Middle,
	Right
};

enum class e_key
{
	First,
	Unknown = First,
	Backspace = 8,
	Tab = 9,
	Enter = 13,
	Shift = 16,
	Control = 17,
	Escape = 27,
	Space = 32,
	Quotation = 39,
	Minus = 45,
	Slash = 47,
	Num0 = 48,
	Num1 = 49,
	Num2 = 50,
	Num3 = 51,
	Num4 = 52,
	Num5 = 53,
	Num6 = 54,
	Num7 = 55,
	Num8 = 56,
	Num9 = 57,
	SemiColon = 59,
	Equal = 61,
	A = 65,
	B = 66,
	C = 67,
	D = 68,
	E = 69,
	F = 70,
	G = 71,
	H = 72,
	I = 73,
	J = 74,
	K = 75,
	L = 76,
	M = 77,
	N = 78,
	O = 79,
	P = 80,
	Q = 81,
	R = 82,
	S = 83,
	T = 84,
	U = 85,
	V = 86,
	W = 87,
	X = 88,
	Y = 89,
	Z = 90,
	LeftBrace = 91,
	BackSlash = 92,
	RightBrace = 93,
	Tilda = 96,
	Last = Tilda
};

class Input
{
private:
	friend class Application;
	
	static Input* m_pInput;

	static void CreateInputManager();
	static void DestroyInputManager();
protected:

public:
	static Input* GetSingleton();
private:
	Vector2 m_oldMousePos;
	bool m_locked;

	enum class e_KeyState
	{
		Pressed,
		Down,
		Released,
		Up
	};

	std::vector<e_KeyState> m_keyState;
	std::vector<e_KeyState> m_buttonState;

	Vector2 m_mousePos;

	Input();
	~Input();

	int timeOut;

	void OnMouseDown(e_mouseButton a_button);
	void OnMouseUp(e_mouseButton a_button);
	void OnKeyDown(const WPARAM& a_param);
	void OnKeyUp(const WPARAM& a_param);

	void Update();
protected:

public:
	/// <summary>Needed for windows callback do not call<summary>
	void FunctionKeyDown(e_key a_button);

	bool IsMousePressed(e_mouseButton a_mouseButton) const;
	bool IsMouseDown(e_mouseButton a_mouseButton) const;
	bool IsMouseReleased(e_mouseButton a_mouseButton) const;
	bool IsMouseUp(e_mouseButton a_mouseButton) const;

	Vector2 GetMousePos() const;
	Vector2 GetDesktopMousePos() const;

	Vector2 GetMouseMovement() const;
	void SetCursorLock(bool a_state);
	bool GetCursorLockState() const;

	void SetCursorVisiblity(bool a_state) const;

	bool IsKeyPressed(e_key a_key) const;
	bool IsKeyDown(e_key a_key) const;
	bool IsKeyReleased(e_key a_key) const;
	bool IsKeyUp(e_key a_key) const;
};