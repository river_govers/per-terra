#pragma once

#include <string>

#include <D3DX11.h>

#include <DirectXMath.h>

struct Vertex
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 normal;

	bool operator ==(const Vertex& a_vert)
	{
		if (pos.x == a_vert.pos.x && pos.y == a_vert.pos.y && pos.z == a_vert.pos.z &&
			normal.x == a_vert.normal.x && normal.y == a_vert.normal.y && normal.z == a_vert.normal.z)
		{
			return true;
		}
		return false;
	}
};

class Texture;

class Model
{
private:
	friend class Pipeline;

	// Do not clear past this point not owned by the model
	ID3D11VertexShader* m_pVertexShader;
	ID3D11PixelShader* m_pPixelShader;

	Texture* m_pTexture;

	void OBJLoader(const char* a_pFilename);
protected:
	int m_indexCount;

	ID3D11Buffer* m_pVertexBuffer;
	ID3D11Buffer* m_pIndiceBuffer;
public:
	Model();
	virtual ~Model();
	
	void SetPixelShader(ID3D11PixelShader* const a_pPixelShader);
	void SetVertexShader(ID3D11VertexShader* const a_pVertexShader);

	void LoadModel(const char* a_pFileName);
	void LoadModel(const std::string& a_fileName);

	int GetIndexCount() const; 

	/// <summary> Creates a new model will destroy the old model <summary>
	static void CreateCube(Model*& a_pModel);
	static void CreateQuadPlane(Model*& a_pModel);
	static void CreateIcoSphere(Model*& a_pModel, int a_recursionLevel = 2);
};