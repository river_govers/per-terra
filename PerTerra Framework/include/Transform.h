#pragma once

#include "Maths/Vector3.h"
#include "Maths/Matrix4.h"

struct Chunk;
class Entity;

class Transform
{
private:
	friend class ChunkManager;

	Vector3 m_translation;
	Vector3 m_rotation;
	Vector3 m_forward;
	Vector3 m_up;
	Vector3 m_right;
	Vector3 m_scale;

	Transform* m_parent;
	Entity* m_entity;
	ChunkManager* m_chunkManager;
	Chunk* m_chunk;

	Vector3 WorldForward() const;
	Vector3 WorldUp() const;
	Vector3 WorldRight() const;

	void Update();
protected:

public:
	Transform();
	Transform(Entity* a_entity, ChunkManager* a_chunkManager);
	~Transform();

	Matrix4 ToMatrix() const;
	explicit operator Matrix4();
	
	void SetParent(Transform& a_parent);

	void SetMatrix(const Matrix4& a_matix);

	Matrix4 operator =(const Matrix4& a_matrix);

	Matrix4 operator *(const Matrix4& a_matrix) const;
	Matrix4 operator *=(const Matrix4& a_matrix);

	Vector3 GetLocalRotation() const;
	Vector3 GetRotation() const;
	Vector3 GetLocalPosition() const;
	Vector3 GetPosition() const;
	Vector3 GetLocalScale() const;
	Vector3 GetScale() const;

	void SetLocalScale(const Vector3& a_scale);

	void Rotate(const Vector3& a_euler);
	void SetLocalRotation(const Vector3& a_euler);

	void Translate(const Vector3& a_translation);
	void SetLocalPosition(const Vector3& a_position);

	Vector3 Forward() const;
	Vector3 Up() const;
	Vector3 Right() const;
};