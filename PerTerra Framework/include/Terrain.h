#pragma once

#include "../include/Model.h"

#include <DirectXMath.h>

#include "Maths/Vector2.h"

class Terrain : public Model
{
private:
	Vertex* m_pVertCache;

	DirectX::XMINT2 m_resolution;
	Vector2 m_size;
protected:

public:
	Terrain(const DirectX::XMINT2& a_resolution, const Vector2& a_size);
	~Terrain();

	/// <summary>Warning expensive use with caution</summary>
	void UpdateBuffer();

	Vertex& GetIndex(int a_x, int a_y);

	DirectX::XMINT2 GetVerticieCount();
	Vector2 GetSize();
	Vector2 GetSpacing();
};