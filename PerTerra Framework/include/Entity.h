#pragma once

#include "Transform.h"

#include "Model.h"

class Entity
{
private:
	friend class ChunkManager;
protected:
	bool m_static;

	Model* m_model = nullptr;
	
	void Draw();
public:
	Transform* m_pTransform;

	Entity(ChunkManager* a_chunkManager, bool a_static = false);
	virtual ~Entity();

	void SetModel(Model* const a_model);

	virtual void Update() { };
};