#pragma once

class Time
{
private:
	friend class Application;

	static float m_deltaTime;
	static float m_scale;
protected:

public:
	static float GetScale();
	static float GetDeltaTime();
	static float GetDeltaTimeUnscaled();

	static void SetScale(float a_scale);
};